import { TranslationsService } from './com/annaniks/zont-client/services/translations.service';
import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { PlatformService } from './com/annaniks/zont-client/services/platform.service';
declare let ga: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],

})
export class AppComponent {
  title = 'zont-client';
  private _letOpenPage: boolean = false;
  constructor(
    private _translationsService: TranslationsService,
    public router: Router,
    private _platformService: PlatformService
  ) {

    this._translationsService.setData().subscribe();
    this._translationsService.translationsHaveGot.subscribe(() => {
      this._letOpenPage = true;
    });
    this.router.events.subscribe(event => {

      if (event instanceof NavigationEnd) {
        if (this._platformService.isPlatformBrowser) {
          ga('set', 'page', event.urlAfterRedirects);
          ga('send', 'pageview');
        }
      }

    });
  }
  get letOpenPage(): boolean {
    return this._letOpenPage
  }

}
