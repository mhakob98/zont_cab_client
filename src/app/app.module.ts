import { AuctionService } from './com/annaniks/zont-client/auction/auction.service';
import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { ApiService } from './com/annaniks/zont-client/services/api.service';
import { AppComponent } from './app.component';
import { MenuItems } from './com/annaniks/zont-client/services/menu-items.service';
import { httpParams } from './com/annaniks/zont-client/params/httpParams';
import { ToastrModule } from 'ngx-toastr';
import "angular2-navigate-with-data";
import { CookieService } from 'ngx-cookie-service';
import { LoginService } from './com/annaniks/zont-client/modals/sign-in/sign-in.service';
import { AuthGuard } from './com/annaniks/zont-client/services/authguard.service';
import { AccountGuard } from './com/annaniks/zont-client/services/accountGuard.service';
import { TranslationsService } from './com/annaniks/zont-client/services/translations.service';
import { TranslatePipe } from './com/annaniks/zont-client/pipes/translations.pipe';
import { TitleService } from './com/annaniks/zont-client/services/title.service';
import { TransferHttpModule, TransferHttpService } from '@gorniv/ngx-transfer-http';
import { GoogleAnalyticsService } from './com/annaniks/zont-client/services/GoogleAnalyticsService';
import { TransferHttpCacheModule } from '@nguniversal/common'; 

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'app-root' }),
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserTransferStateModule,
    TransferHttpCacheModule,
    HttpClientModule,
    TransferHttpModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    TransferHttpService,
    AuctionService,
    MenuItems,
    {
      provide: 'BASE_URL', useValue: httpParams.baseUrl
    },
    ApiService,
    CookieService,
    LoginService,
    AuthGuard,
    AccountGuard,
    TranslationsService,
    TranslatePipe,
    TitleService,
    GoogleAnalyticsService

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
