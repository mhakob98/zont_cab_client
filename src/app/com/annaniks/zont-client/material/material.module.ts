import { NgModule } from '@angular/core';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDatepickerModule,} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatDialogModule} from '@angular/material/dialog';
import {MatMenuModule} from '@angular/material/menu';
import {MatSidenavModule} from '@angular/material/sidenav';


@NgModule({
    imports:[MatTabsModule,MatDatepickerModule,MatNativeDateModule,MatDialogModule,MatMenuModule,MatSidenavModule],
    exports:[MatTabsModule,MatDatepickerModule,MatNativeDateModule,MatDialogModule,MatMenuModule,MatSidenavModule],
})
export class MaterialModule{

}