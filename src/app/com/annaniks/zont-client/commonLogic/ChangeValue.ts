export class ChangeValue {
  public onValueChange(givenForm, formsError, validationMessages, data?) {
    console.log(validationMessages, formsError);

    if (!givenForm) return;
    let form = givenForm;
    for (let field in formsError) {
      formsError[field] = "";
      let control = form.get(field);
      if (control && control.dirty && !control.valid) {
        let message = validationMessages[field];
        for (let key in control.errors) {
          console.log(message);
          return message[key]
        }
      }
    }
  }
}