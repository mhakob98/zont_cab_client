import { duration } from "moment";

export interface LoginResponse {
  accessToken: string;
  refreshToken: string;
}
export interface ClientLogin {
  username: string;
  password: string;
}
export interface ClientLoginResponse {
  accessToken: string;
  refreshToken: string;
}
export interface GetStarted {
  title: string;
  backgroundColor: string;
  textColor: string;
}
export interface CardResponse {
  id: string;
  holderName: string;
  cardNumber: string;
  cvv: string;
  expireDate: string;
  type: string;
}
export interface Role {
  role: string;
}
export interface LoginForm {
  username: string;
  password: string;
}
export interface Preorder{
  additionalComments: string
  canDriversWatch: boolean
  carType: string
  client: Client
  company: string
  currentPrice:number
  destination: string
  distance: number
  driver: string
  duration: number
  endAddress: string
  id: number
  isFixedPrice: boolean
  isPaymentSuccessful: boolean
  maxPrice: number
  minPrice: number
  offerEndTime: string
  partner?: string
  priceChanged: string
  priceInterval: number
  remainingTime: number
  startAddress: string
  startDate: string
  startPoint: string
  status: string
  terminal: string
  timerStopped: boolean
  totalAmount: number
  tripType: string
}
export interface Client{
  balance: number
  clientRating: number
  countryOfResidancy: string
  currency: string
  dateOfBirth: string
  deal: number
  deviceToken?: string
  email: string
  firstName: string
  gender: string
  id: string
  imagePath: string
  isActivated: boolean
  isExternal: boolean
  lastName: string
  myReferalCode: string
  nationality: string
  phoneNumber: string
  promoCode: string
  referalCode: string
  referalUserCount: number
  uuid: string
}
export interface RiderForm {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  confirmPassword: string;
  dateOfBirth: string;
  gender: string;
  phoneNumber: string;
  referalCode: string;
  privacyPolicy: boolean;
  bankCards: any;
}
export interface RiderSignUpResponse {
  id: string;
  phoneVerificationToken: string;
}
export interface CompanyForm {
  firstName: string;
  lastName: string;
  email: string;
  gender: string;
  password: string;
  referalId: string;
  dateOfBirth: string;
}
export interface CompanySignUpResponse {
  id: string;
  phoneVerificationToken: string;
}
export interface Coordinate {
  latitude: number;
  longitude: number;
}
export interface AuctionDistanceResponse {
  tripType: string;
  amount: number;
  distance: number;
  maxAmount: number;
  minAmount: number;
  isActive: boolean;
}
export class PointsAddresses {
  origin: string;
  destination: string;
}
export interface MakeAuction {
  startPointLatitude: number;
  startPointLongitude: number;
  startDate: string;
  duration: any;
  destination: string;
  clientPrice: number;
  tripType: string;
  carType: string;
  distance: number;
  terminal: string;
  cardId:string
}
export interface Auction {
  addresses: {
    destination: string;
    origin: string;
  };
  date: string;
  destination: string;
  distance: number;
  pickupPointLatitude: number;
  pickupPointLongitude: number;
  time: string;
  tripInfo: Array<Trip>;
  carType?: string;
  hours?:number
}
export interface Trip {
  amount: number;
  distance: number;
  isActive: boolean;
  maxAmount: number;
  minAmount: number;
  tripType?: string;
  tripTypes?:string;
  perMinuteForTime?:number;
  baseFare?:number
}
export interface RequestParams {
  headers?;
  observe?;
  responseType?;
}
