import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";

@Injectable()
export class CommonActionsService{
    constructor(private _apiService:ApiService){}
    public cancelOrder(orderId):Observable<any>{
        return this._apiService.delete(`/Auction/cancel/${orderId}`)
    }
}