import { ApiService } from './api.service';
import { Injectable } from "@angular/core";
import { TranslationsService } from './translations.service';
import { Observable,of } from 'rxjs';
@Injectable()
export class TitleService{
    
    public titlesInCurrentLanguage:{};
    
    constructor(private _apiService:ApiService,private _translationService:TranslationsService){
        _apiService.get("/assets/titles.json",false,'','',null,true).subscribe(titles=>{            
            switch (_translationService._currentLanguageKey) {
                case 19:
                    this.titlesInCurrentLanguage = titles[0]
                    break;
                case 20:
                    this.titlesInCurrentLanguage  = titles[1];
                    break;
                case 14:
                    this.titlesInCurrentLanguage = titles[2]
            }
        })
    }
}