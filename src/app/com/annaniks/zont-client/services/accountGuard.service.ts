import { Injectable } from "@angular/core";
import { CanActivate, Router } from '@angular/router';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
@Injectable()
export class AccountGuard implements CanActivate {
    constructor(private _apiService: ApiService, private _router: Router) { }
    canActivate(): Observable<boolean> | Promise<boolean> | boolean {
        if (!this._apiService.isAuthorized) {
            this._router.navigate([''])
        }
        return this._apiService.isAuthorized;
    }
}