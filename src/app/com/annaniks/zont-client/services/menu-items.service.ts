import { Injectable } from '@angular/core';
@Injectable()
export class MenuItems{
    public menuItems: any;
    private defaultMenuItems = [
        {
            "label": "_drive",
            "name":"",
            "routerLink": "/become-driver",
            'popUpOpen': ''
        },
        {
            "label": "_rides",
            "name":"",
            "routerLink": "/how-it-works",
            'popUpName': ''
        },
        {
            "label": "countries",
            "name":"",
            "routerLink": "",
            'popUpName': ''
        },
        {
            "label": "_help",
            "name":"",
            "routerLink": "/hotels-b2b-tours-operators",
            'popUpName': ''
        },
    ]
    constructor() {}
    changeItems(arg = this.defaultMenuItems) {
        this.menuItems = arg
    }


}