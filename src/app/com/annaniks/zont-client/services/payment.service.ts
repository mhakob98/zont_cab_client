import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { Injectable } from "@angular/core";
import { CardResponse } from '../models/models';

@Injectable()
export class PaymentService{
    constructor(private _apiService:ApiService){  }

    public addCard(cardId:string):Observable<any>{
        return this._apiService.post(`/Client/cards?cardSource=${cardId}`,{},"response","text")
    }
    public getCards():Observable<CardResponse[]>{
        return this._apiService.get("/Client/cards",true)
    }
    public deleteCard(cardId:string):Observable<any>{
        return this._apiService.delete(`/Client/cards/${cardId}`,"response","text")
    }
}