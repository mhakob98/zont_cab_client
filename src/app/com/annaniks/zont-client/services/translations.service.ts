import { ApiService } from "./api.service";
import { Injectable, Injector } from "@angular/core";
import { forkJoin, Subject } from "rxjs";
import { map } from "rxjs/operators";
import { PlatformService } from "./platform.service";
@Injectable()
export class TranslationsService {
  public currentLanguage: string[];
  public translates: object = {};
  public _currentLanguageKey: number = undefined;
  public languages: Array<object> = [];
  private _translateKeys: any = [];
  private _translationsHaveGot = new Subject<void>();

  constructor(
    private _apiService: ApiService,
    private injector: Injector,
    private _platformService: PlatformService
  ) { }

  public setData() {
    if (this._platformService.isPlatformBrowser) {
      this.currentLanguage = window.location.host.split(".");
    }
    if (this._platformService.isPlatformServer) {
      let req = this.injector.get('request');
      this.currentLanguage = req.get('host').split(".");
    }
    return forkJoin(this.getLanguages(), this._getTranslateKeys()).pipe(
      map((response: any) => {
        this.languages.forEach((language: object, index: number) => {
          if (language["languageKey"].toLowerCase() == this.currentLanguage[0].toLowerCase()) {
            this._currentLanguageKey = language["id"];
          }
        });
        if (!this._currentLanguageKey) {
          this.languages.forEach((language: any) => {
            if (language.languageKey == "EN") {
              this._currentLanguageKey = language["id"];
            }
          })
        }
        return this.getTranslate(this._translateKeys, this._currentLanguageKey);
      })
    );
  }

  public getTranslateKeys() {
    return this._apiService.get("/TranslationKeys");
  }

  public getTranslate(key, language) {
    return this._apiService
      .post(
        `/Translations/client/bykeynames/${language}/`,
        `"${key.toString()}"`
      )
      .subscribe((data: any) => {
        for (let i of data) {
          this.translates[i.translationKeys.keys] = i.text;
        }
        this._translationsHaveGot.next();
      });
  }

  public getTranslateValue(key: string) {
    return this.translates[key];
  }

  public getLanguages() {
    return this._apiService.get("/Language").pipe(
      map((languages: Array<object>) => {
        this.languages = languages;
        return languages
      })
    );
  }

  get translationsHaveGot(): Subject<void> {
    return this._translationsHaveGot
  }

  private _getTranslateKeys() {
    return this.getTranslateKeys().pipe(
      map((translateKeys: Array<object>) => {
        let count = 0;
        for (let i of translateKeys) {
          this._translateKeys[count] = i["keys"];
          count++;
        }
      })
    );
  }
}
