import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformServer, isPlatformBrowser } from '@angular/common';

@Injectable({
    providedIn: 'root'
})
export class PlatformService {
    private _isPlatformServer: boolean;
    private _isPlatformBrowser: boolean;

    constructor(@Inject(PLATFORM_ID) private _platformId) {
        this._isPlatformServer = isPlatformServer(this._platformId);
        this._isPlatformBrowser = isPlatformBrowser(this._platformId);
    }

    public get isPlatformServer(): boolean {
        return this._isPlatformServer;
    }

    public get isPlatformBrowser(): boolean {
        return this._isPlatformBrowser;
    }
}