import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { of, Observable } from 'rxjs';
import { RequestParams } from '../models/models';
import { CookieService } from 'ngx-cookie-service';
import { TransferHttpService } from '@gorniv/ngx-transfer-http'

@Injectable()
export class ApiService {
    private _isAuthorized: boolean = false;
    constructor(
        private _httpClient: TransferHttpService,
        private _cookieService: CookieService,
        private _router: Router,
        @Inject('BASE_URL') private _baseUrl: string,

    ) { }

    /**
     * 
     * @param url - request url
     * @param observe - httpOption for get full response,not required
     * @param responseType - httpOption for get full response on type text
     */
    public get(url: string, authorization?: boolean, observe?, responseType?, loginToken?, standalonePath?: boolean): Observable<Object | any> {
        let token = this._cookieService.get('Client_accessToken') || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
        })
        if (authorization && !loginToken) {
            headers = headers.append('authorization', "Bearer " + token);
        }
        if (loginToken) {
            headers = headers.append('authorization', "Bearer " + loginToken);
        }
        let params: RequestParams = { headers: headers };
        if (observe == 'response')
            params.observe = 'response';
        if (responseType == 'text')
            params.responseType = 'text';

        return this._httpClient.get(standalonePath ? url : this._baseUrl + url, params)
    }


    /**
     * 
     * @param url - request url, 
     * @param body - sending object
     * @param observe - httpOption for get full response
     * @param responseType 
     */
    public post(url: string, body: object | string, observe?, responseType?): Observable<Object | any> {
        let token = this._cookieService.get('Client_accessToken') || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'authorization': 'Bearer ' + token,
        })
        let params: RequestParams = { headers: headers };
        if (observe == 'response')
            params.observe = 'response';
        if (responseType == 'text')
            params.responseType = 'text';
        return this._httpClient.post(this._baseUrl + url, body, params);
    }

    public postFormData(url: string, formData: FormData, observe?, responseType?): Observable<Object | any> {
        let token = this._cookieService.get('Client_accessToken') || '';
        let headers = new HttpHeaders({
            'authorization': 'Bearer ' + token,
        })
        let params: RequestParams = { headers: headers };
        if (observe == 'response')
            params.observe = 'response';
        if (responseType == 'text')
            params.responseType = 'text';

        return this._httpClient.post(this._baseUrl + url, formData, params);
    }

    /**
     * 
     * @param url 
     * @param body 
     * @param observe 
     * @param responseType 
     */
    public put(url: string, body: object, observe?, responseType?): Observable<Object | any> {
        let token = this._cookieService.get('Client_accessToken') || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'authorization': 'Bearer ' + token,
        })
        let params: RequestParams = { headers: headers };
        if (observe == 'response')
            params.observe = 'response';
        if (responseType == 'text')
            params.responseType = 'text';

        return this._httpClient.put(this._baseUrl + url, body, params);
    }

    /**
     * 
     * @param url 
     * @param observe 
     * @param responseType 
     */
    public delete(url: string, observe?, responseType?): Observable<Object | any> {
        let token = this._cookieService.get('Client_accessToken') || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'authorization': 'Bearer ' + token,
        })
        let params: RequestParams = { headers: headers };
        if (observe == 'response')
            params.observe = 'response';
        if (responseType == 'text')
            params.responseType = 'text';

        return this._httpClient.delete(this._baseUrl + url, params);
    }

    /**
     * 
     */
    public checkToken(): Observable<boolean> {
        return this.get('/Login/getrole', true, 'response', 'text')
            .pipe(
                map(() => {
                    this._isAuthorized = true
                    return true;
                }),
                catchError((err, caught) => {
                    this._isAuthorized = false;
                    return this._getToken();
                }))
    }

    private _getToken(): Observable<boolean> {
        let refreshToken = this._cookieService.get('Client_refreshToken') || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
        })
        return this._httpClient.post(this._baseUrl + `/Login/getaccesstoken?refreshToken=${refreshToken}`, { headers: headers })
            .pipe(map((data) => {
                this._isAuthorized = true
                this._updateCookies(data);
                return true;
            }),
                catchError((err, caught) => {
                    this._isAuthorized = false
                    return of(true);
                }))
    }
    /**
     * 
     * @param data 
     */
    private _updateCookies(data): void {
        this._cookieService.set('Client_accessToken', data.token);
        this._cookieService.set('Client_refreshToken', data.refreshToken);
    }
    get isAuthorized(): boolean {
        return this._isAuthorized;
    }
    public setIsAuthorized(value): void {
        this._isAuthorized = value
    }
}