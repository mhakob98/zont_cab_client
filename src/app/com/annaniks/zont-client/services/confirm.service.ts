import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { ConfirmModal } from "../modals/confirm-modal/confirm-modal";

@Injectable()
export class ConfirmService{
    constructor(private _matDialog: MatDialog) { }

    public openConfirmModal(): Observable<boolean> {
        let dialogRef = this._matDialog.open(ConfirmModal, {
            width: '450px',
            height: '250px'
        })
        return dialogRef.afterClosed().pipe(
            map((data) => {
                if (data && data.confirm) {
                    return data.confirm;
                }
                else{
                    return false;
                }
            })
        )
    }
}

