import { PaymentService } from './../../services/payment.service';
import { EventEmitter, Output } from '@angular/core';
import { CardResponse } from './../../models/models';
import { Component, OnInit, Input } from '@angular/core';
const CARDTYPES:object = {
  'Visa': 'assets/cards/visa.png',
  'Visa (debit)': 'assets/cards/visaDebit.png',
  'MasterCard': 'assets/cards/mastercard.png',
  'MasterCard (2-series)': 'assets/cards/mastercard.png',
  'MasterCard (debit)': 'assets/cards/mastercardDebit.png',
  'American Express': 'assets/cards/american.png',
  'Discover': 'assets/cards/discover.png',
  'Diners Club':'assets/cards/dinersClub.png',
  'JCB': 'assets/cards/jcb.png',
  'UnionPay': 'assets/cards/unionPay.png'
}
@Component({
  selector: 'bank-card',
  templateUrl: './bank-card.component.html',
  styleUrls: ['./bank-card.component.scss']
})
export class BankCardComponent implements OnInit {
  @Input() card:CardResponse;
  @Input('value') private _value:string = '';
  @Output('getValue') private _eventEmitter:EventEmitter<string> = new EventEmitter<string>();
  @Output() cardWasDeleted:EventEmitter<void> = new EventEmitter<void>();
  @Input('setValue') 
  private _selectedValue:string;
  private _cardtypes:object = CARDTYPES
  set setValue($event){
      this._selectedValue = $event;
  }
  constructor(private _paymentService:PaymentService ) { }

  ngOnInit() {
    
  }
  
  public onChange($event):void{
    this._eventEmitter.emit($event);
  }
  
  public deleteBankCard(bankCardId:string):void{
    this._paymentService.deleteCard(bankCardId).subscribe(was=>{
      this.cardWasDeleted.emit()
      
    })
  }
  get value():string{
    return this._value;
  }
  get selectedValue():string{
    return this._selectedValue;
  }
  set selectedValue(value){
    this._selectedValue = value;
  }
  get cardTypes():object{
    return this._cardtypes
  }
}
