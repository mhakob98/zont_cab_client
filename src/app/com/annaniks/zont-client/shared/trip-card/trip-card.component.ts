import { Preorder } from './../../models/models';
import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import * as _moment from 'moment';
import { CommonActionsService } from '../../services/common-actions.service';
import { ConfirmService } from '../../services/confirm.service';

const moment = _moment;
const distanceInfo = [
  {
    name: "_preorder_distance",
    value: "distance"
  },
  {
    name: "_preorder_duration",
    value: "4 minutes"
  },
  {
    name: "Category",
    value: "carType"
  },
  {
    name: "Offer ends",
    value: "Sat,28 July,2018",
    time: "18:22"
  },
]
@Component({
  selector: 'trip-card',
  templateUrl: './trip-card.component.html',
  styleUrls: ['./trip-card.component.scss']
})
export class TripCardComponent implements OnInit {
  @Input() preorderInfo:Preorder
  @Output() tripeCanceled:EventEmitter<void> = new EventEmitter<void>()
  public showOverlay:boolean = false;
  asd = new Date('2019/02/24 08:00:00')
  public distanceInfo:Array<object> = distanceInfo;
  constructor(private _commonActionsService:CommonActionsService,private _confirmService:ConfirmService) { }

  ngOnInit() {
    this.preorderInfo.startDate = moment.utc(this.preorderInfo.startDate, "DD/MM/YYYY HH:mm:ss").local().format("DD/MM/YYYY HH:mm:ss")
  }
  public formatDate(date:string):string{
    if(!date){
      return 'The trip is not completed yet'
    }
    var finalFormat = ''
    var day = date.split(" ")[0].split("/").reverse();
    day.forEach(element => {
      finalFormat += element + "/"
    });
    return finalFormat  
  }
  public cancelOrder(id):void{
    this.showOverlay = true;
    this._confirmService.openConfirmModal().subscribe(response=>{
        if(response == true){
        this._commonActionsService.cancelOrder(id).subscribe(() =>{
            this.tripeCanceled.emit()
            this.showOverlay = false;
          })
        }
    })
  
  }
}
