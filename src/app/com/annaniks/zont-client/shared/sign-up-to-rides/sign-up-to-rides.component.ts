import { ApiService } from './../../services/api.service';
import { LoginService } from './../../modals/sign-in/sign-in.service';
import { Component, OnInit, Output ,EventEmitter} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SignUpService } from '../../modals/sign-up/sign-up.service';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { RiderSignUpResponse, LoginResponse } from '../../models/models';
import { CookieService } from 'ngx-cookie-service';
@Component({
    selector: 'sign-up-to-rides',
    templateUrl: './sign-up-to-rides.component.html',
    styleUrls: ['./sign-up-to-rides.component.scss'],
})
export class SignUpToRidesComponent implements OnInit {

    @Output() clientSignedUp:EventEmitter<boolean> = new EventEmitter<boolean>();
    private _signUpForm: FormGroup;
    private _riderSubscription: Subscription = new Subscription();
    public  matchPasswords:boolean = false;
    private _showOverlay:boolean = false;
    constructor(private _cookieService:CookieService ,private _fb: FormBuilder,private _signUpService:SignUpService,private _toastr: ToastrService,private _loginService:LoginService,private _apiService:ApiService) { }

    ngOnInit() {
        this._formBuilder();        
    }

    private _formBuilder(): void {
        this._signUpForm = this._fb.group({
            firstName: ["", [Validators.required, Validators.minLength(3)]],
            lastName: ["", [Validators.required, Validators.minLength(3)]],
            email: ["", [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
            password: ["", [Validators.required, Validators.minLength(6)]],
            confirmPassword: ["", [Validators.required, Validators.minLength(6)]],
            dateOfBirth:["12/03/2019",],
            gender: ["male", ],
            phoneNumber:["",],
            referalCode:["",],
            privacyPolicy:["",Validators.requiredTrue],
            bankCards:[null]
          },{ validator: this._matchingPasswords('password', 'confirmPassword') });
          this._signUpForm.valueChanges.subscribe(data=>{
            if(data.password !== data.confirmPassword && this._signUpForm.get("password").dirty && this._signUpForm.get("confirmPassword").dirty ){
                this.matchPasswords = true;
              }
              if(data.password === data.confirmPassword && this._signUpForm.get("password").dirty && this._signUpForm.get("confirmPassword").dirty ){
                this.matchPasswords = false;
              }
             
          })
    }
    private _matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
        return (group: FormGroup): { [key: string]: any } => {
          let password = group.controls[passwordKey];
          let confirmPassword = group.controls[confirmPasswordKey];
          if (password.value !== confirmPassword.value) {
            return {
              mismatchedPasswords: true
            };
          }
        }
      }
      private _setCookies(key: string, value: string, domain: string = ''): void {
        this._cookieService.set(key, value,null,null, domain);
      }
    private _riderSignUp():void {
         
        this._showOverlay = true;
        this._riderSubscription = this._signUpService.riderSignUp(this._signUpForm.value).subscribe((data: RiderSignUpResponse) => {
            this._signUpService.sendRiderVerifyMessage(this._signUpForm.value.email).subscribe((data: string) => {
            this._loginService.clientLogin({username:this._signUpForm.value["email"],password:this._signUpForm.value["password"]}).subscribe((loginResponse: LoginResponse)=>{
                this._apiService.setIsAuthorized(true)
                this._setCookies("Client_accessToken", loginResponse.accessToken);
                this._setCookies("Client_refreshToken", loginResponse.refreshToken);
                this._actionsAfterSignUp(this._signUpForm.value.email)
            })
            },error=>{this._handleSignUpFail(Object.values(error.error)[0][0])})
        },error=>{this._handleSignUpFail(Object.values(error.error)[0][0])})
    }
    private _actionsAfterSignUp(email:string):void{
        this._showOverlay = false
        // this._signUpDoneMessage(email)
        this.clientSignedUp.emit(true)
        this.signUpForm.reset()
    }
    private _handleSignUpFail(errorMessage:string):void{
        this._showOverlay = false;
        this._signUpFailMessage(errorMessage)
    }
    private _signUpFailMessage(errorMessage:string):void{
        this._toastr.error('',errorMessage,{
          positionClass:'toast-top-center'
        })
    }
    private _signUpDoneMessage(email:string):void {
        this._toastr.success('', 'Verification code sent to email'+email,{
          positionClass:'toast-top-center'
        });
    }
    public onRiderSignUp():void{
        this._signUpForm.valid ? this._riderSignUp() : null
    }
    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this._riderSubscription.unsubscribe();
    }
    get signUpForm(): FormGroup {
        return this._signUpForm;
    }
    get showOverlay():boolean{
        return this._showOverlay;
    }

}