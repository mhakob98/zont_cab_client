import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SignInModal } from '../../modals/sign-in/sign-in.modal';
import { SignUpModal } from '../../modals/sign-up/sign-up.modal';
import { GetStarted } from '../../models/models';


@Component({
  selector: 'driver-get-started',
  templateUrl: './driver-get-started.component.html',
  styleUrls: ['./driver-get-started.component.scss'],
})
export class DriverGetStartedComponent implements OnInit {
  @Input() getStartedInformation:GetStarted;
  constructor(public dialog: MatDialog) {}
  
  ngOnInit(){    
  }
  private _openSignInDialog(): void {    
    const dialogRef = this.dialog.open(SignInModal, {
      width: '1100px',
      panelClass:'sign_in_modal'
    });
    dialogRef.afterClosed().subscribe(result => {

    });
   
  }
  private _openSignUpDialog(): void {
    const dialogRef = this.dialog.open(SignUpModal,{
      panelClass:'sign_up_modal'
    })
    dialogRef.afterClosed().subscribe(result => {
      if(result==false){
        this._openSignInDialog();
      }
    });
  }
  public onOpenSignUp():void{
    this._openSignUpDialog()
  }
}
