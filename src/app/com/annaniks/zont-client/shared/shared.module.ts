import { PaginatorComponent } from './paginator/paginator.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { InternationalPhoneModule } from 'ng4-intl-phone';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgSelectModule } from '@ng-select/ng-select';
import { HowBecomePartnerComponent } from './how-become-partner/how-become-partner.component';
import { DriverGetStartedComponent } from './driver-get-started/driver-get-started.component';
import { TripCardComponent } from './trip-card/trip-card.component';
import { RidesOverviewComponent } from './rides-overview/rides-overview.component';
import { SearchingComponent } from './searching/searching.component';
import { MatIconModule, MatDatepickerModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatButtonToggleModule, MatSelectModule, MatRadioModule } from '@angular/material';
import { AccountTabComponent } from './account-tab/account-tab.component';
import { SignInPageComponent } from './sign-in-page/sign-in-page.component';
import { SignUpToRidesComponent } from './sign-up-to-rides/sign-up-to-rides.component';
import { BankCardComponent } from './bank-card/bank-card.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { InViewportModule } from 'ng-in-viewport';
import { TranslatePipe } from '../pipes/translations.pipe';
import { DigitOnlyDirective } from '../directives/onlyDigit.directive';
import { SecondsToDatePipe } from '../pipes/seconds-to-date.pipe';

@NgModule({
  declarations: [
    HowBecomePartnerComponent,
    DriverGetStartedComponent,
    TripCardComponent,
    RidesOverviewComponent,
    SignUpToRidesComponent,
    SignInPageComponent,
    AccountTabComponent,
    SearchingComponent,
    BankCardComponent,
    PaginatorComponent,
    TranslatePipe,
    DigitOnlyDirective,
    SecondsToDatePipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatButtonToggleModule,
    RouterModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatRadioModule,
    MatIconModule,
    MatSelectModule,
    InternationalPhoneModule,
    MatInputModule,
    GooglePlaceModule,
    NgSelectModule,
    ReactiveFormsModule,
    ScrollToModule.forRoot(),
    InViewportModule

  ],
  providers: [
  ],
  exports: [
    InternationalPhoneModule,
    GooglePlaceModule,
    NgSelectModule,
    FormsModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    RidesOverviewComponent,
    MatIconModule,
    MatInputModule,
    SignUpToRidesComponent,
    MatDatepickerModule,
    AccountTabComponent,
    ReactiveFormsModule,
    MatSelectModule,
    SignInPageComponent,
    HowBecomePartnerComponent,
    DriverGetStartedComponent,
    TripCardComponent,
    SearchingComponent,
    MatRadioModule,
    BankCardComponent,
    PaginatorComponent,
    ScrollToModule,
    InViewportModule,
    TranslatePipe,
    DigitOnlyDirective,
    SecondsToDatePipe
  ],
  entryComponents: []
})
export class SharedModule { }
