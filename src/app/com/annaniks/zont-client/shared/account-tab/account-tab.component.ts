import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Auction, Trip } from '../../models/models';

const TABLINKS:Array<string>=["Car  Class","Options","Sign In","Checkout"];


@Component({
  selector: 'account-tab',
  templateUrl: './account-tab.component.html',
  styleUrls: ['./account-tab.component.scss']
})
export class AccountTabComponent implements OnInit {
  @Input() auctionInfo:Auction;
  @Output() selectedTripType: EventEmitter<Trip> = new EventEmitter<Trip>();
  @Output() onAccountMake:EventEmitter<boolean>= new EventEmitter<boolean>();
  @Input() activeLink: string;
  @Input() tabLinks:Array<string>;
  // private _tabLinks = TABLINKS;
  constructor() { }
  ngOnInit() {

  }
  public onTabLinkClick(link:string):void{
    this.activeLink = link;
  }
  ngAfterViewInit(): void {

  }
  public onSelectedTripTypeChange(tripType:Trip):void{
    this.selectedTripType.emit(tripType);
  }
  public accountMakeTransfer():void{    
    this.onAccountMake.emit(true)
  }
  // get tabLinks():Array<string>{
  //   return this._tabLinks;
  // }
  // get activeLink():string{
  //   return this.activeLink;
  // }

}