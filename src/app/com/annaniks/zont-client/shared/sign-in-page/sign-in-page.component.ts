import { ApiService } from './../../services/api.service';
import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import { LoginService } from '../../modals/sign-in/sign-in.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoginForm, LoginResponse, Role } from '../../models/models';
import { Subscription } from 'rxjs';
import { ChangeValue } from '../../commonLogic/ChangeValue';
import { CookieService } from 'ngx-cookie-service';
import { MenuItems } from '../../services/menu-items.service';


@Component({
  selector: 'sign-in-page',
  templateUrl: './sign-in-page.component.html',
  styleUrls: ['./sign-in-page.component.scss'],
})
export class SignInPageComponent implements OnInit {
  private _subscription: Subscription = new Subscription();
  private _showOverlay:boolean = false;
  private _loginForm: FormGroup;
  private _changeValue = new ChangeValue();
  @Output() clientLoggedIn:EventEmitter<boolean> = new EventEmitter<boolean>();

  private _formsError = {
    "username": "",
    "password": "",
  }
  private _validationMessages = {
    "username": {
      "required": "email_is_required",
      "email": "wrong_format_of_email"
    },
    "password": {
      "required": "password_is_required",
      "minlength": "password_min_length_6_char"
    },
  }
  constructor(
    @Inject(FormBuilder) private _formBuilderInstance: FormBuilder,
    private _socialAuthService: AuthService,
    private _loginService: LoginService,
    private _cookieService: CookieService,
    private _router: Router,
    private _toastr: ToastrService,
    private _apiService:ApiService,
    private _menuItems:MenuItems
    ) { }


  public ngOnInit() {
    this._formBuilder();
  }
  public onClickLogin(): void {
    this._loginForm.valid ? this._login(this._loginForm.value) : null
  }
  public onClickSocialLogin(socialPlatform: string):void{
    this._socialSignIn(socialPlatform);
  }
  private _login(loginForm: LoginForm): void {
    this._showOverlay = true;
    this._subscription = this._loginService.clientLogin(loginForm).subscribe((loginResponse: LoginResponse) => {
      this._apiService.setIsAuthorized(true)
      this._changeMenuItems()
      this._setCookies("Client_accessToken", loginResponse.accessToken);
      this._setCookies("Client_refreshToken", loginResponse.refreshToken);
      this.clientLoggedIn.emit(true);
      this._showOverlay = false;
    },error=>{this._handleSignInFail(Object.values(error.error)[0][0])})
  }
  private _socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform == "facebook") {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this._socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        let data:object;
        if (socialPlatform !== "google") {
          data = { userId: userData.id, facebookAccessToken: userData.authToken }
        } else{
          data = {idToken:userData.idToken}
        }
        this._loginService.loginWithSocialPages(data,socialPlatform).subscribe((data: LoginResponse) => {   
          this._apiService.setIsAuthorized(true)
          this._changeMenuItems()
          this._setCookies("role", "Client");
          this._setCookies("Client_accessToken", data.accessToken)
          this._setCookies("Client_refreshToken", data.refreshToken)
          // this._checkIfPreOrder();
        })
      }
    );
  }
  private _formBuilder() {
    this._loginForm = this._formBuilderInstance.group({
      username: ["", [Validators.required]],
      password: ["", [Validators.required, Validators.minLength(6)]],
    });
    this._loginForm.valueChanges.subscribe((data) => {
      this._changeValue.onValueChange(this._loginForm, this._formsError, this._validationMessages, data)
    })
    this._changeValue.onValueChange(this._loginForm, this._formsError, this._validationMessages)
  }
  private _setCookies(key: string, value: string, domain: string = ''): void {
    this._cookieService.set(key, value,null,null, domain);
  }


  private _handleSignInFail(errorMessage:string):void{
    this._showOverlay = false;
    this._signInFailMessage(errorMessage)
  }
  private _signInFailMessage(errorMessage:string):void{
    this._toastr.error('',errorMessage,{
      positionClass:'toast-top-center'
    })
  }
  // Check if authorized and change top navigation bar items
  private _changeMenuItems():void{
    if(this._apiService.isAuthorized){
      
      this._menuItems.changeItems([
        {
          "label": "_account",
          "name":"",
          "routerLink": "/personal-area",
          'popUpName': ''
      },
      {
        "label":"_rides_authorized",
        "name":"",
        "routerLink":"/rides",
        "popUpName":""
       },
       {
        "label": "countries",
        "name":"",
        "routerLink": "",
        'popUpName': ''
       },
       {
        "label": "_help",
        "name":"",
        "routerLink": "/hotels-b2b-tours-operators",
        'popUpName': ''
       }
    ])
    }
  }
  public ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  get loginForm():FormGroup{
    return this._loginForm
  }
  get showOverlay():boolean{
    return this._showOverlay;
  }
}