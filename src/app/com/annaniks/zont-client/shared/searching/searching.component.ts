import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Output,EventEmitter, Inject } from "@angular/core";
import { MomentDateAdapter } from "@angular/material-moment-adapter";
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE
} from "@angular/material/core";
import * as _moment from "moment";
const status = [
  {
    value: "completed",
    name: "_finished"
  },
  {
    value: "",
    name: "_show_all"
  },
  {
    value: "upcoming",
    name: "_upcoming"
  }
];
const moment = _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: "L"
  },
  display: {
    dateInput: "L",
    monthYearLabel: "MMMM YYYY",
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMMM YYYY"
  }
};
@Component({
  selector: "searching",
  templateUrl: "./searching.component.html",
  styleUrls: ["./searching.component.scss"],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]
})
export class SearchingComponent implements OnInit {
  public selected: string = "";
  private _searchForm:FormGroup;
  @Output() searchEvent:EventEmitter<object> = new EventEmitter<object>();
  private _startDate;
  private _endDate;
  public sortingCards: boolean = false;
  public statuses = status;
  constructor(
    @Inject(FormBuilder) private _formBuilderInstance: FormBuilder,
  ) {}

  ngOnInit() {
    this._formBuilder()
  }
  private _formatDate(date:any):string{
    return date ? `${date._i.date < 10 ? "0" + date._i.date : date._i.date }/${date._i.month + 1 < 10 ? "0" + (date._i.month + 1): date._i.month + 1}/${date._i.year}` : ""
  }
  public emitSearchEvent():void{    
    this.searchEvent.emit({startDate:this._formatDate(this.startDate),endDate:this._formatDate(this.endDate)})
  }
  private _formBuilder() {
    this._searchForm = this._formBuilderInstance.group({
      status: "",
      sort: "",
    });
    this._searchForm.valueChanges.subscribe((data)=>{
      
    })
  }
  public popUpForSortCards(): void {
    this.sortingCards = !this.sortingCards;
  }
  set startDate(startDate) {
    this._startDate = startDate;
  }
  get startDate() {
    return this._startDate;
  }
  set endDate(endDate) {
    this._endDate = endDate;
  }
  get endDate() {
    return this._endDate;
  }
  get searchForm():FormGroup{
    return this._searchForm;
  }
}