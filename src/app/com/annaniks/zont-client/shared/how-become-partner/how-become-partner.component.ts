import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'how-become-partner',
  templateUrl: './how-become-partner.component.html',
  styleUrls: ['./how-become-partner.component.scss'],
})
export class HowBecomePartnerComponent implements OnInit {
  @Input() howBecomeData;
  constructor() { }

  ngOnInit() {
  }

}
