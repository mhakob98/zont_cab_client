import { ApiService } from './../../services/api.service';
import { MenuItems } from './../../services/menu-items.service';
import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SignInModal } from '../../modals/sign-in/sign-in.modal';
import { SignUpModal } from '../../modals/sign-up/sign-up.modal';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { GoogleAnalyticsService } from '../../services/GoogleAnalyticsService';

@Component({
  selector: 'top-navigation-bar',
  templateUrl: './top-navigation-bar.component.html',
  styleUrls: ['./top-navigation-bar.component.scss'],

})
export class TopNavigationBarComponent implements OnInit {
  private _languages: any;
  private _sidenavState: boolean = false;
  private _options: FormGroup;
  private _showLanguage: boolean = false;
  @ViewChild("sidenav") sidenav;
  public _authorizedLinks: object[] = [
    { id: 1, name: 'Gyumri' },
    { id: 2, name: 'Erevan' },
    { id: 3, name: 'Ijevan' },
    { id: 4, name: 'Vanadzor' },
    { id: 5, name: 'Sevan' }
  ];

  ngOnInit() {
    if (this._apiService.isAuthorized) {

      this.menuItems.changeItems([
        {
          "label": "_account",
          "name": "",
          "routerLink": "/personal-area",
          'popUpName': ''
        },
        {
          "label": "_rides_authorized",
          "name": "",
          "routerLink": "/rides",
          "popUpName": ""
        },
        {
          "label": "countries",
          "name": "",
          "routerLink": "",
          'popUpName': ''
        },
        {
          "label": "_help",
          "name": "",
          "routerLink": "/hotels-b2b-tours-operators",
          'popUpName': ''
        }
      ])
    } else {
      this.menuItems.changeItems();
    }
    this._getLanguages();
  }
  constructor(public dialog: MatDialog,
    public menuItems: MenuItems,
    private _router: Router,
    private _apiService: ApiService,
    private _cookieService: CookieService,
    private _fb: FormBuilder,
    public googleAnalyticsService: GoogleAnalyticsService
  ) {
    this._options = _fb.group({
      bottom: 0,
      fixed: true,
      top: 80
    });
  }
  public handleSidenavStateChange() {
    this._sidenavState = !this._sidenavState;
  }
  public openSignInDialog(): void {
    this.googleAnalyticsService.eventEmitter("reg", "signIn", "userLabel", 1);

    const dialogRef = this.dialog.open(SignInModal, {
      width: '1100px',
      panelClass: 'sign_in_modal'
    });
    dialogRef.afterClosed().subscribe(result => {
    });

  }
  public navigateTo(navigationLink: string): void {

    this._router.navigate([navigationLink]);
    this.sidenav.toggle();
    this._showLanguage = false;
  }
  public openSignUpDialog(): void {
    this.googleAnalyticsService.eventEmitter("reg", "signUp", "userLabel", 1);

    const dialogRef = this.dialog.open(SignUpModal, {
      panelClass: 'sign_up_modal'
    })
    dialogRef.afterClosed().subscribe(result => {
      if (result == false) {
        this.openSignInDialog();
      }
    });
  }
  public changeLanguage(language?): void {
    switch (language) {
      case "RU":
        window.location.href = "http://ru.zont.cab"
        break;
      case "EN":
        window.location.href = "http://en.zont.cab"
        break;
      case "FR":
        window.location.href = "http://fr.zont.cab"
        break;
    }
  }
  public toggleShowLanguage(): void {
    this._showLanguage = !this.showLanguage;
  }
  private _getLanguages(): any {
    let tempLanguage;
    return this._apiService.get("/Language").subscribe((languages: any) => {
      this._languages = languages
      this._languages.forEach((element, index: number) => {
        if (element["languageKey"].toLowerCase() == window.location.host.split('.')[0].toLowerCase()) {
          tempLanguage = this._languages[0]
          this._languages[0] = element
          this._languages[index] = tempLanguage
        }
      })
      if (!tempLanguage) {
        this._languages.forEach((element, index: number) => {
          if (element["languageKey"].toLowerCase() == "en") {
            tempLanguage = this._languages[0]
            this._languages[0] = element
            this._languages[index] = tempLanguage
          }
        });
      };
    })
  }

  public logOutFromSystem(): void {
    this._cookieService.deleteAll();
    window.location.reload()
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    event.target.innerWidth;
    if (event.target.innerWidth > 1024 && this.sidenav._opened) {
      this.sidenav.toggle()
      this._showLanguage = false;
    }
  }

  public goHome(): void {
    this._router.navigate([''])
  }
  get apiService(): ApiService {
    return this._apiService
  }
  get authorizedLinks(): object[] {
    return this._authorizedLinks
  }
  get languages(): any {
    return this._languages
  }
  get options(): FormGroup {
    return this._options;
  }
  get sidenavState() {
    return this._sidenavState;
  }
  get showLanguage(): boolean {
    return this._showLanguage;
  }
}
