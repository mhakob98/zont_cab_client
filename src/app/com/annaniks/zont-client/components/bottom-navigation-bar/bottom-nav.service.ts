import { Observable } from 'rxjs';
import { ApiService } from './../../services/api.service';
import { Injectable } from '@angular/core';
@Injectable()
export class BottomNavService{
    
    constructor(private _apiService:ApiService){}
    
    public _getAllCountries():Observable<any>{
        return this._apiService.get("/City/client/19")
    }

}