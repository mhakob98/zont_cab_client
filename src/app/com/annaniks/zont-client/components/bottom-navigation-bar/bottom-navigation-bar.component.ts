import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { BottomNavService } from './bottom-nav.service';

@Component({
  selector: 'bottom-navigation-bar',
  templateUrl: './bottom-navigation-bar.component.html',
  styleUrls: ['./bottom-navigation-bar.component.scss'],
  providers:[BottomNavService]
})
export class BottomNavigationBarComponent implements OnInit {
  public selectedCity:object;
  public _allCities:Array<{id:number,name:string,routerLink:string}> =[];

  constructor(private _bottomNavService:BottomNavService,private _router:Router) { }

  ngOnInit() {
    this.getAllCities();
  }
  private getAllCities():void{
    this._bottomNavService._getAllCountries().subscribe(cities=>{
      cities.forEach((element,index) => {
        this._allCities = [...this._allCities, {
          id:index,
          name:element.city.key,
          routerLink:`/cities/${element.city.country.id}/${element.city.id}`
        }];
      });  
    })
  }
  public navigateTo(city:{id:number,name:string,routerLink:string}):void{    
    this._router.navigate([`${city.routerLink}`]);
  }
  get allCities():any{
    return this._allCities;
  }
}
