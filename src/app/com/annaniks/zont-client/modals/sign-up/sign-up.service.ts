import { Observable } from 'rxjs';
import { ApiService } from './../../services/api.service';
import { Injectable } from "@angular/core";
import { RiderForm, RiderSignUpResponse, CompanyForm } from '../../models/models';

@Injectable()
export class SignUpService{
    constructor(private _apiService:ApiService){}

    public riderSignUp(riderForm:RiderForm):Observable<RiderSignUpResponse>{
        return this._apiService.post("/client",riderForm) 
    }

    public sendRiderVerifyMessage(email:string):Observable<string>{
        return this._apiService.get(`/Verification/clientVerifyEmail?email=${email}&host=http://zont.cab`)
    }

    public companySignUp(companyForm:CompanyForm):Observable<any>{
        return this._apiService.post("/CompanyRegister/register",companyForm)
    }
    public sendCompanyVerifyMessage(email:string):Observable<string> {
        return this._apiService.get(`/Verification/companyVerifyEmail?email=${email}&host=http://zont.cab`)
    }
}