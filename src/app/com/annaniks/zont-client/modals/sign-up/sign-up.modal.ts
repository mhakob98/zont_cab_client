import { Component, OnInit, Inject, QueryList, ViewChildren, NgZone } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { SubSink } from 'subsink';
import { AuthService } from 'angularx-social-login';
import { SignUpService } from './sign-up.service';
import { ChangeValue } from '../../commonLogic/ChangeValue';
import { RiderSignUpResponse } from '../../models/models';
import { ToastrService } from 'ngx-toastr';
import { TranslatePipe } from '../../pipes/translations.pipe';
declare var Stripe: any;

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.modal.html',
  styleUrls: ['./sign-up.modal.scss'],
  providers: [DatePipe],
})
export class SignUpModal implements OnInit {
  @ViewChildren("internationalPhone") internationalPhone !: QueryList<any>;
  private _subs = new SubSink();

  private _changeValue = new ChangeValue();
  public rideForm: FormGroup;
  private _apiKey = 'pk_test_HOcUc1D1u7A8tA1juMHr1Hrz';
  private _stripe: any;
  public matchPasswords: boolean = false;
  public matchPasswordsCompany: boolean = false;
  public companyForm: FormGroup;
  public showOverlay: boolean = false;
  private _rideFormError = {
    "firstName": "",
    "lastName": "",
    "email": "",
    "password": "",
    "confirmPassword": "",
    "phoneNumber": "",
  }
  private _rideFormValidationMessages = {
    "firstName": {
      "required": "first_name_is_required",
    },
    "lastName": {
      "required": "last_name_is_required",
    },
    "email": {
      "required": "email_is_required",
      "pattern": "wrong_email_pattern"
    },
    "password": {
      "required": "password_is_required",
      "minlength": "password_min_length_6_char"
    },
    "confirmPassword": {
      "required": "confirm_password_is_required"
    },
    "phoneNumber": {
      "required": "phone_number_is_required"
    },
  }
  private _companyFormError = {
  }
  private _companyFormValidationMessages = {
  }
  constructor(
    @Inject(FormBuilder) private _formBuilderInstance: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data,
    public _dialogRef: MatDialogRef<SignUpModal>,
    private _signUpService: SignUpService,
    private _toastr: ToastrService,
    private _datePipe: DatePipe,
    private _translationPipe: TranslatePipe,
    private _zone: NgZone
  ) { }
  public ngOnInit() {
    this._stripe = Stripe(this._apiKey);
    this._rideFormBuilder();
    this._companyFormBuilder();
  }
  public ngAfterViewInit(): void {
    this._setPlaceholder('first');
    this._setPlaceholder('last')
  }
  private _rideFormBuilder(): void {
    this.rideForm = this._formBuilderInstance.group({
      firstName: ["", [Validators.required, Validators.minLength(3)]],
      lastName: ["", [Validators.required, Validators.minLength(3)]],
      email: ["", [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      password: ["", [Validators.required, Validators.minLength(6)]],
      confirmPassword: ["", [Validators.required, Validators.minLength(6)]],
      dateOfBirth: ["12/03/2019",],
      gender: ["male"],
      phoneNumber: ["", [Validators.required]],
      referalCode: ["",],
      privacyPolicy: ["", Validators.requiredTrue],
      bankCards: [null]
    }, { validator: this._matchingPasswords('password', 'confirmPassword') });

    this.rideForm.valueChanges.subscribe((data) => {
      console.log(this.rideForm.value);

      if (data.password !== data.confirmPassword && this.rideForm.get("password").dirty && this.rideForm.get("confirmPassword").dirty) {
        this.matchPasswords = true;
      }
      if (data.password === data.confirmPassword && this.rideForm.get("password").dirty && this.rideForm.get("confirmPassword").dirty) {
        this.matchPasswords = false;
      }
    })
  }
  private _companyFormBuilder(): void {
    this.companyForm = this._formBuilderInstance.group({
      firstName: [null, [Validators.required, Validators.minLength(3)]],
      lastName: [null, [Validators.required, Validators.minLength(3)]],
      companyName: [null, [Validators.required, Validators.minLength(3)]],
      address: [null,],
      email: [null, [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      password: [null, [Validators.required, Validators.minLength(6)]],
      confirmPassword: [null, [Validators.required, Validators.minLength(6)]],
      country: ["france", [Validators.required, Validators.minLength(3)]],
      licenseTypeId: [9, Validators.required],
      companyTypeId: [9, Validators.required],
      phone: ["", [Validators.required, Validators.maxLength(19)]],
      privacyPolicy: [false, [Validators.requiredTrue]]
    }, { validator: this._matchingPasswords('password', 'confirmPassword') });

    this.companyForm.valueChanges.subscribe((data) => {
      this._changeValue.onValueChange(this.companyForm, this._companyFormError, this._companyFormValidationMessages, data)
      if (data.password !== data.confirmPassword && this.companyForm.get("password").dirty && this.companyForm.get("confirmPassword").dirty) {
        this.matchPasswordsCompany = true;
      }
      if (data.password === data.confirmPassword && this.companyForm.get("password").dirty && this.companyForm.get("confirmPassword").dirty) {
        this.matchPasswordsCompany = false;
      }
    })
    this._changeValue.onValueChange(this.companyForm, this._companyFormError, this._companyFormValidationMessages)
  }
  private _matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];
      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }
  private _riderSignUp(): void {
    if (this.rideForm.value["phoneNumber"].substring(0, 3) === '+33' && this.rideForm.value["phoneNumber"][3] === '0') {

      let length = this.rideForm.value["phoneNumber"].length;
      let start = this.rideForm.value["phoneNumber"].slice(0, 3);
      let end = this.rideForm.value["phoneNumber"].slice(4, length);
      this.rideForm.value['phoneNumber'] = `${start}${end}`


    }
    // this._rideForm.value["dateOfBirth"] = this._datePipe.transform(this._rideForm.value["dateOfBirth"],'dd/MM/yyyy')    
    this.showOverlay = true;
    this._subs.add(this._signUpService.riderSignUp(this.rideForm.value).subscribe((data: RiderSignUpResponse) => {
      this._signUpService.sendRiderVerifyMessage(this.rideForm.value.email).subscribe((data: string) => {
        this._actionsAfterSignUp(this.rideForm.value.email)
      }, error => { this._handleSignUpFail(Object.values(error.error)[0][0]) })
    }, error => { this._handleSignUpFail(Object.values(error.error)[0][0]) }))
  }
  private async _companySignUp() {
    if (this.companyForm.value["phone"].substring(0, 3) === '+33' && this.companyForm.value["phone"][3] === '0') {
      let length = this.companyForm.value["phone"].length;
      let start = this.companyForm.value["phone"].slice(0, 3);
      let end = this.companyForm.value["phone"].slice(4, length);
      this.companyForm.value['phone'] = `${start}${end}`
    }
    let formValue = this.companyForm.value
    const token = await this._stripe.createToken('account', {
      business_type: 'company',
      company: {
        name: formValue.companyName,
        address: {
          line1: formValue.address,
        },
      },
      tos_shown_and_accepted: true,
    }
    );
    if (token.token) {
      this.companyForm.value['accountToken'] = token.token.id
    }
    this.showOverlay = true;
    this._subs.add(this._signUpService.companySignUp(this.companyForm.value).subscribe((data: any) => {
      this._signUpService.sendCompanyVerifyMessage(this.companyForm.value.email).subscribe((data: string) => {
        this._actionsAfterSignUp(this.companyForm.value.email)
      }, error => { this._handleSignUpFail(Object.values(error.error)[0][0]) })
    }, error => { this._handleSignUpFail(Object.values(error.error)[0][0]) }))
  }

  private _signUpDoneMessage(email: string): void {
    this._toastr.success('', 'Verificatoin code sent to email' + email, {
      positionClass: 'toast-top-center'
    });
  }

  private _handleSignUpFail(errorMessage: string): void {
    this.showOverlay = false;
    this._signUpFailMessage(errorMessage)
  }

  private _signUpFailMessage(errorMessage: string): void {
    this._toastr.error('', errorMessage, {
      positionClass: 'toast-top-center'
    })
  }

  private _actionsAfterSignUp(email: string): void {
    this.showOverlay = false
    this._signUpDoneMessage(email)
    this._dialogRef.close();
  }

  private _setPlaceholder(whichInstance: string): void {
    this.internationalPhone[whichInstance]["phoneComponent"].nativeElement.firstChild.children[1].placeholder = this._translationPipe.transform("_sign_up_mobile_phone")
  }

  public onCloseDialog(state?: boolean): void {
    this.closeDialog(state)
  }

  public onRiderSignUp(): void {
    if (this.rideForm.valid) {
      this._riderSignUp()
    } else {
      console.log(this.rideForm, this._rideFormError, this._rideFormValidationMessages);

      let validationError = this._changeValue.onValueChange(this.rideForm, this._rideFormError, this._rideFormValidationMessages)
      if (validationError) {
        this._signUpFailMessage(this._translationPipe.transform(validationError))
      } else {
        this._signUpFailMessage(this._translationPipe.transform("fill_all_required_fields"))
      }
    }
  }

  public onCompanySignUp(): void {
    this.companyForm.valid ? this._companySignUp() : null
  }

  public closeDialog(state?: boolean): void {
    state == false ? this._dialogRef.close(false) : this._dialogRef.close()
  }


  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }
}
