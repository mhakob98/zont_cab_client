import { ApiService } from './../../services/api.service';
import { Router } from '@angular/router';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoginService } from './sign-in.service';
import { LoginResponse, Role, LoginForm } from '../../models/models';
import { Subscription } from 'rxjs';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ChangeValue } from '../../commonLogic/ChangeValue';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angularx-social-login';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';

@Component({
  templateUrl: './sign-in.modal.html',
  styleUrls: ['./sign-in.modal.scss'],
})
export class SignInModal implements OnInit {
  private _subscription: Subscription = new Subscription();
  private _showOverlay: boolean = false;
  private _loginForm: FormGroup;
  private _changeValue = new ChangeValue();
  private _formsError = {
    "username": "",
    "password": "",
  }
  private _validationMessages = {
    "username": {
      "required": "email_is_required",
      "email": "wrong_format_of_email"
    },
    "password": {
      "required": "password_is_required",
      "minlength": "password_min_length_6_char"
    },
  }
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    @Inject(FormBuilder) private _formBuilderInstance: FormBuilder,
    private _socialAuthService: AuthService,
    private _dialogRef: MatDialogRef<SignInModal>,
    private _loginService: LoginService,
    private _cookieService: CookieService,
    private _router: Router,
    private _toastr: ToastrService,
    private _apiService: ApiService
  ) { }


  public ngOnInit() {
    this._formBuilder();
    this._socialAuthService.authState.subscribe((userData) => {
      console.log(userData);
      if (userData) {
        let data: object;
        if (userData.provider !== "GOOGLE") {
          data = { userId: userData.id, facebookAccessToken: userData.authToken }
        } else {
          data = { idToken: userData.idToken }
        }
        this._loginService.loginWithSocialPages(data, userData.provider.toLocaleUpperCase()).subscribe((data: LoginResponse) => {
          this._setCookies("role", "Client");
          this._setCookies("Client_accessToken", data.accessToken)
          this._setCookies("Client_refreshToken", data.refreshToken)
          this._checkIfPreOrder();
          this._apiService.setIsAuthorized(true)
          this._dialogRef.close();

        })
      }
    });
  }
  public onClickLogin(): void {
    this._loginForm.valid ? this._login(this._loginForm.value) : null
  }
  // public onClickSocialLogin(socialPlatform: string): void {
  //   this._socialSignIn(socialPlatform);
  // }
  public onClickCloseDialog(): void {
    this._closeDialog()
  }
  private _login(loginForm: LoginForm): void {
    this._showOverlay = true
    this._subscription = this._loginService.userLogin(loginForm).subscribe((loginResponse: LoginResponse) => {
      this._showOverlay = false;
      this._loginService.getRole(loginResponse.accessToken).subscribe((role: Role) => {
        if (role.role.toLowerCase() == "client") {

          this._setCookies(role.role + "_role", role.role);
          this._setCookies(role.role + "_accessToken", loginResponse.accessToken);
          this._setCookies(role.role + "_refreshToken", loginResponse.refreshToken)
          this._apiService.setIsAuthorized(true)
          this._checkIfPreOrder();
          this._dialogRef.close();
        }
        else {
          this._setCookies(role.role + "_role", role.role, "zont.cab");
          this._setCookies(role.role + "_accessToken", loginResponse.accessToken, "zont.cab")
          this._setCookies(role.role + "_refreshToken", loginResponse.refreshToken, "zont.cab")
          this._decideHrefLocation(role);
        }
      })
    }, error => { this._handleSignInFail(Object.values(error.error)[0][0]) })
  }
  // private _socialSignIn(socialPlatform: string) {
  //   let socialPlatformProvider;
  //   if (socialPlatform == "facebook") {
  //     socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
  //   } else if (socialPlatform == "google") {
  //     socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
  //   }
  //   console.log(socialPlatformProvider);

  //   this._socialAuthService.signIn(socialPlatformProvider).then(
  //     (userData) => {
  //       console.log(userData);
  //       let data: object;
  //       if (socialPlatform !== "google") {
  //         data = { userId: userData.id, facebookAccessToken: userData.authToken }
  //       } else {
  //         data = { idToken: userData.idToken }
  //       }
  //       this._loginService.loginWithSocialPages(data, socialPlatform).subscribe((data: LoginResponse) => {
  //         this._setCookies("role", "Client");
  //         this._setCookies("Client_accessToken", data.accessToken)
  //         this._setCookies("Client_refreshToken", data.refreshToken)
  //         this._checkIfPreOrder();
  //       })
  //     }
  //   );
  // }
  signInWithGoogle(): void {
    this._socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this._socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }


  private _formBuilder() {
    this._loginForm = this._formBuilderInstance.group({
      username: ["", [Validators.required]],
      password: ["", [Validators.required, Validators.minLength(6)]],
    });
    this._loginForm.valueChanges.subscribe((data) => {
      this._changeValue.onValueChange(this._loginForm, this._formsError, this._validationMessages, data)
    })
    this._changeValue.onValueChange(this._loginForm, this._formsError, this._validationMessages)
  }
  private _setCookies(key: string, value: string, domain: string = ''): void {
    this._cookieService.set(key, value, null, null, domain);
  }
  private _checkIfPreOrder(): void {
    if (!this.data) {
      // window.location.reload();
    }
    else {
      this._router.navigate([this._router.url], { queryParams: { tripInfo: JSON.stringify(this.data.tripInfo), auctionInfo: JSON.stringify(this.data.auctionInfo) } })
      // window.location.reload();
    }
  }
  private _decideHrefLocation(role: Role): void {
    role.role.toLowerCase() == "partner" ? window.location.href = 'http://' + role.role.toLowerCase() + '.zont.cab/dashboard/default' :
      window.location.href = 'http://' + role.role.toLowerCase() + '.zont.cab/' + role.role.toLowerCase() + '/dashboard'
  }
  private _handleSignInFail(errorMessage: string): void {
    this._showOverlay = false;
    this._signInFailMessage(errorMessage)
  }
  private _signInFailMessage(errorMessage: string): void {
    this._toastr.error('', errorMessage, {
      positionClass: 'toast-top-center'
    })
  }
  private _closeDialog(): void {
    this._dialogRef.close();
  }
  public ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }
  get loginForm(): FormGroup {
    return this._loginForm;
  }
  get showOverlay(): boolean {
    return this._showOverlay
  }

}
