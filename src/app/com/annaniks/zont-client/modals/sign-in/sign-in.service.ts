import { Role, LoginForm, LoginResponse, ClientLoginResponse, ClientLogin } from './../../models/models';
import { Observable } from 'rxjs';
import { ApiService } from './../../services/api.service';
import { Injectable } from "@angular/core";

@Injectable()
export class LoginService {
    constructor(private _apiService: ApiService) { }


    public userLogin(userInfo: LoginForm): Observable<LoginResponse> {
        return this._apiService.post("/login", userInfo);
    }
    public clientLogin(loginInfo: ClientLogin): Observable<ClientLoginResponse> {
        return this._apiService.post("/login/client", loginInfo)
    }
    public getRole(token: string): Observable<Role> {
        return this._apiService.get("/login/getrole", true, null, null, token)
    }

    public loginWithSocialPages(data: object, provider: string): Observable<LoginResponse> {
        if (provider !== "GOOGLE") {
            return this._apiService.post("/client/facebookLogin", data).pipe()
        }
        else {
            return this._apiService.post("/client/googleLogin", data).pipe()
        }
    }

}
