import { Component, OnInit, Inject, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChangeValue } from '../../commonLogic/ChangeValue';
import { PartnerSignUpService } from './partner-sign-up.service';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { TranslatePipe } from '../../pipes/translations.pipe';

@Component({
  templateUrl: './partner-sign-up.modal.html',
  styleUrls: ['./partner-sign-up.modal.scss'],
  providers:[PartnerSignUpService],
})
export class PartnerSignUpModal implements OnInit {
  @ViewChild("internationalPhone") internationalPhone;
  private _partnerSignUpForm: FormGroup;
  private _changeValue = new ChangeValue();
  private _showOverlay:boolean = false;

  private _formsError = {
    "email": "",
    "password": "",
    "firstName": "",
    "lastName": "",
    "country": "",
    "referalId": "",

  }
  private _validationMessages = {
    "email": {
      "required": "Email is Required",
      "email": "Wrong format of email"
    },
    "password": {
      "required": "Password is Required",
      "minlength": "Min length of password must be 6 characters"
    },
    "firstName": {
      "required": "First Name is required",
      "minlength": "Min length of first name must be 3 characters"

    },
    "lastName": {
      "required": "First Name is required",
      "minlength": "Min length of last name must be 3 characters"

    },
    "referalId": {
      "required": "Referal id is required",
      "minlength": "Min length of Referal id must be 2 characters"
    },
    
  }
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private _dialogRef: MatDialogRef<PartnerSignUpModal>,
    @Inject(FormBuilder) private _formBuilderInstance: FormBuilder,
    private _cookieService: CookieService,
    private _partnerSignUpService:PartnerSignUpService,
    private _toastr: ToastrService,
    private _translationPipe:TranslatePipe
  ) { }

  public ngOnInit() {
    this._formBuilder();
  }
  public ngAfterViewInit(): void {    
    this._setPlaceholder();
  }
  private _formBuilder() {
    this._partnerSignUpForm = this._formBuilderInstance.group({
      firstName: ["", [Validators.required, Validators.minLength(3)]],
      lastName: ["", [Validators.required, Validators.minLength(3)]],
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(6)]],
      phone:["",[Validators.required,Validators.maxLength(19)]],
      referalId: "",     
      privacyPolicy:[false,[Validators.requiredTrue]]
    });
    this._partnerSignUpForm.valueChanges.subscribe((data) => {
      this._changeValue.onValueChange(this._partnerSignUpForm, this._formsError, this._validationMessages, data)
    })
    this._changeValue.onValueChange(this._partnerSignUpForm, this._formsError, this._validationMessages)
  }
  private _setCookies(key: string, value: string, domain: string = ''): void {
    this._cookieService.set(key, value,null,null,domain);
  }
  private _partnerSignUp() {
    this._showOverlay = true;

    this._partnerSignUpService.partnerSignUp(this._partnerSignUpForm.value).subscribe((data)=>{
      this._actionsAfterSignUp(this._partnerSignUpForm.value.email)
      this._partnerSignUpService.sendPartnerVerifyMessage(this._partnerSignUpForm.value.email).subscribe(info => {
      },error=>{this._handleSignUpFail(Object.values(error.error)[0][0])})
    },error=>{this._handleSignUpFail(Object.values(error.error)[0][0])})
  }
  public onPartnerSignUp():void{
    this._partnerSignUpForm.valid ? this._partnerSignUp() : null
  }
  private _signUpDoneMessage(email:string):void {
    this._toastr.success('', 'Verificatoin code sent to email'+email,{
      positionClass:'toast-top-center'
    });
  }
  private _handleSignUpFail(errorMessage:string):void{
    this._showOverlay = false;
    this._signUpFailMessage(errorMessage)
  }
  private _signUpFailMessage(errorMessage:string):void{
    this._toastr.error('',errorMessage,{
      positionClass:'toast-top-center'
    })
  }
  private _actionsAfterSignUp(email:string):void{
    this._showOverlay = false
    this._signUpDoneMessage(email)
    this._dialogRef.close();
  }
  private _setPlaceholder():void{
    this.internationalPhone["phoneComponent"].nativeElement.firstChild.children[1].placeholder = this._translationPipe.transform("_sign_up_mobile_phone")

  }
  private _closeDialog(state ? :boolean ): void {
    state == false ? this._dialogRef.close(false) : this._dialogRef.close()
  }
  public onCloseDialog(state ? :boolean ):void{
    this._closeDialog(state)
  }
  get partnerSignUpForm():FormGroup{
    return this._partnerSignUpForm
  }
  get showOverlay():boolean{
    return this._showOverlay;
  }
}
