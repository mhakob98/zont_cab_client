import { Observable } from 'rxjs';
import { ApiService } from './../../services/api.service';
import { Injectable } from "@angular/core";
@Injectable()
export class PartnerSignUpService{
    constructor(private _apiService:ApiService){}

    public partnerSignUp(PartnerForm){
        return this._apiService.post("/partner",PartnerForm) 
    }

    public sendPartnerVerifyMessage(email:string):Observable<string>{
        return this._apiService.get(`/Verification/partnerVerifyEmail?email=${email}&host=http://zont.cab`)
    }
}