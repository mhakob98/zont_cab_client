import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AuctionDistanceResponse, PointsAddresses, Coordinate } from '../../models/models';
import { Router } from '@angular/router';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { AuctionService } from '../auction.service';
import { ToastrService } from 'ngx-toastr';
import { TranslatePipe } from '../../pipes/translations.pipe';
import * as _moment from 'moment';
import { PlatformService } from '../../services/platform.service';

const moment = _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'L',
  },
  display: {
    dateInput: 'L',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'auction-actions',
  templateUrl: './auction-actions.component.html',
  styleUrls: ['./auction-actions.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class AuctionActionsComponent implements OnInit {
  @Input() isInCitiesView: boolean = false;
  public hours = ["1 hours", "2 hours", "3 hours", "4 hours", "5 hours", "6 hours", "7 hours", "8 hours", "9 hours", "10 hours", "11 hours", "12 hours", "13 hours"];
  private _pickUpPoint: Coordinate;
  private _pickUpPointHourly: Coordinate;
  private _dropPoint: Coordinate;
  private _tripInfo: PointsAddresses = new PointsAddresses();
  private _tripInfoHourly: PointsAddresses = new PointsAddresses();
  ua = window.navigator.userAgent.toLowerCase();
  isWinPhone = this.ua.indexOf('windows phone') !== -1;
  isIOS = !this.isWinPhone && !!this.ua.match(/ipad|iphone|ipod/);
  private _showOverlay: boolean = false;
  public date: any;
  private _minDate: Date = new Date(Date.now());
  public isBrowser: boolean;

  @ViewChild("picker") picker;
  @ViewChild('pickerHourly') pickerHourly;
  private _auctionForm: FormGroup;
  private _auctionFormHourly: FormGroup;
  constructor(
    private _router: Router,
    private _auctionService: AuctionService,
    private _toastr: ToastrService,
    private _fb: FormBuilder,
    private _translatePipe: TranslatePipe,
    private _platformService: PlatformService
  ) {
    this.isBrowser = this._platformService.isPlatformBrowser;
  }


  ngOnInit() {
    // this._minDate.setHours(15)
    this._formBuilder();
    this._hourlyFormBuilder();
  }

  public ngAfterViewInit() {
    this.picker._dateAdapter._localeData.firstDayOfWeek = 1;
    this.picker._dateAdapter._localeData.narrowDaysOfWeek = [
      this._translatePipe.transform("Sun"),
      this._translatePipe.transform("Mon"),
      this._translatePipe.transform("Tue"),
      this._translatePipe.transform("Wed"),
      this._translatePipe.transform("Thu"),
      this._translatePipe.transform("Fri"),
      this._translatePipe.transform("Sat"),
    ]
  }

  public handlePickupChange(pickupPoint: any, hourly?: boolean) {
    if (!hourly) {
      this._pickUpPoint = {
        latitude: pickupPoint.geometry.location.lat(),
        longitude: pickupPoint.geometry.location.lng()
      }
      this._tripInfo.origin = pickupPoint.formatted_address;
    } else {
      this._pickUpPointHourly = {
        latitude: pickupPoint.geometry.location.lat(),
        longitude: pickupPoint.geometry.location.lng()
      }
      this._tripInfoHourly.origin = pickupPoint.formatted_address;


    }

  }

  public handleDropChange(dropPoint: any) {
    this._dropPoint = {
      latitude: dropPoint.geometry.location.lat(),
      longitude: dropPoint.geometry.location.lng()
    }
    this._tripInfo.destination = dropPoint.formatted_address;
  }

  public routeToAuctionDetail(hourly: boolean): void {
    if (!hourly) {
      this.date = this._auctionForm.value["startdate"];
      this._showOverlay = true
      this._auctionService.getAuctionInformation([this._pickUpPoint, this._dropPoint]).subscribe((tripInfo: AuctionDistanceResponse) => {
        this._showOverlay = false
        this._router.navigateByData({
          url: ['auction'],
          data: {
            tripInfo: tripInfo,
            addresses: {
              origin: this._tripInfo["origin"],
              destination: this._tripInfo["destination"],
            },
            date: `${this.date._i.date < 10 ? "0" + this.date._i.date : this.date._i.date}/${this.date._i.month + 1 < 10 ? "0" + (this.date._i.month + 1) : this.date._i.month + 1}/${this.date._i.year}`,
            time: this._auctionForm.get("startTime").value,
            distance: tripInfo[0].distance,
            duration: tripInfo[0].duration,
            pickupPointLatitude: this._pickUpPoint.latitude,
            pickupPointLongitude: this._pickUpPoint.longitude,
            destination: `${this._dropPoint.latitude},${this._dropPoint.longitude}`
          }
        });

      }, (error: any) => {
        this._showOverlay = false;
        this._auctionFailMessage("We don't work in these cities")
      })
    } else {
      this.date = this._auctionFormHourly.value["startDate"];
      this._showOverlay = true
      this._auctionService.getHourlyAuctionInformation(this._pickUpPointHourly).subscribe(tripInfo => {
        this._showOverlay = false;
        this._router.navigateByData({
          url: ['auction'],
          data: {
            tripInfo: tripInfo,
            addresses: {
              origin: this._tripInfoHourly["origin"],
            },
            date: `${this.date._i.date < 10 ? "0" + this.date._i.date : this.date._i.date}/${this.date._i.month + 1 < 10 ? "0" + (this.date._i.month + 1) : this.date._i.month + 1}/${this.date._i.year}`,
            time: this._auctionFormHourly.get("startTime").value,
            pickupPointLatitude: this._pickUpPointHourly.latitude,
            pickupPointLongitude: this._pickUpPointHourly.longitude,
            hours: +this._auctionFormHourly.value['duration'].split(' ')[0]
          }
        });
      }, (error: any) => {
        this._showOverlay = false;
        this._auctionFailMessage("We don't work in these cities Hourly")
      })
    }

  }

  private _auctionFailMessage(errorMessage: string): void {
    this._toastr.error('', errorMessage, {
      positionClass: 'toast-top-center'
    })
  }


  private _formBuilder(): void {
    this._auctionForm = this._fb.group({
      pickUpPoint: [null, Validators.required],
      dropPoint: [null, Validators.required],
      startdate: [null, Validators.required],
      startTime: [null, Validators.required]
    })
  }
  private _hourlyFormBuilder(): void {
    this._auctionFormHourly = this._fb.group({
      duration: [null, Validators.required],
      startDate: ['', Validators.required],
      startTime: ['', Validators.required]
    })
    this._auctionFormHourly.valueChanges.subscribe(data => {
    })
  }
  get auctionForm(): FormGroup {
    return this._auctionForm;
  }
  get auctionFormHourly(): FormGroup {
    return this._auctionFormHourly;
  }
  get minDate(): Date {
    return this._minDate
  }
  get showOverlay(): boolean {
    return this._showOverlay;
  }
}
