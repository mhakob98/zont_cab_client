import { MaterialModule } from './../material/material.module';
import { NgModule } from '@angular/core';
import { AuctionComponent } from './auction/auction.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { AuctionActionsComponent } from './auction-actions/auction-actions.component';

@NgModule({
    declarations: [AuctionComponent, AuctionActionsComponent],
    imports: [MaterialModule, SharedModule, FormsModule, ReactiveFormsModule, CommonModule, NgxMaterialTimepickerModule.forRoot()],
    exports: [AuctionComponent, AuctionActionsComponent],

})
export class AuctionsModule { }