import { Injectable } from "@angular/core";
import { ApiService } from "../services/api.service";
import { Observable } from "rxjs";
import { Coordinate,AuctionDistanceResponse, MakeAuction } from "../models/models";
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable()
export class AuctionService{
    constructor(private _apiService:ApiService,public httpClient:HttpClient){}

    public getAuctionInformation(coordinates:Array<Coordinate>):Observable<AuctionDistanceResponse>{
        return this._apiService.post("/PreorderDistance/driverTypestwo",coordinates)
    }
    public getHourlyAuctionInformation(coordinates:Coordinate):Observable<any>{
        return this._apiService.post("/Distance/nearDriverTypesWithStatus?radius=15000000",coordinates)
    }
    public makeAuction(auctionInfo:MakeAuction):Observable<any>{
        return this._apiService.post('/Auction/addAuction',auctionInfo);
    }
}