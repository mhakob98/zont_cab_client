import { TranslationsService } from './../services/translations.service';
import { Pipe, PipeTransform } from '@angular/core';
import { Observer, Observable } from 'rxjs';
@Pipe({
    name: "translate"
})
export class TranslatePipe implements PipeTransform {
    constructor(private translateService: TranslationsService) {
    }

    transform(key) {                    
        return this.translateService.getTranslateValue(key)
    }

}
