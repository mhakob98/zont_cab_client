import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: "secToDate"
})
export class SecondsToDatePipe implements PipeTransform {
    transform(seconds: number) {
        var date = new Date(0, 0, 0, 0, 0, 0, 0);
        date.setSeconds(seconds * 60);
        return date;
    }

}
