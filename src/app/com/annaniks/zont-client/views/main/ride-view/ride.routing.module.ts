import { RideViewComponent } from './ride-view.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

let rideRouting: Routes = [
    {path:'',component:RideViewComponent}
]

@NgModule({
    imports: [RouterModule.forChild(rideRouting)],
    exports: [RouterModule]
})
export class RideRoutingModule { }