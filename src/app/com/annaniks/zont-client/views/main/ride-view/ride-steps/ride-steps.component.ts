import { Component, OnInit } from '@angular/core';
import { SignInModal } from '../../../../modals/sign-in/sign-in.modal';
import { SignUpModal } from '../../../../modals/sign-up/sign-up.modal';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'ride-steps',
  templateUrl: './ride-steps.component.html',
  styleUrls: ['./ride-steps.component.scss'],
})
export class RideStepsComponent implements OnInit {

  constructor(public dialog: MatDialog) {}

  ngOnInit() {
  }
  private _openSignInDialog(): void {    
    const dialogRef = this.dialog.open(SignInModal, {
      width: '1100px',
      panelClass:'sign_in_modal'
    });
    dialogRef.afterClosed().subscribe(result => {

    });
   
  }
  private _openSignUpDialog(): void {
    const dialogRef = this.dialog.open(SignUpModal,{
      panelClass:'sign_up_modal'
    })
    dialogRef.afterClosed().subscribe(result => {
      if(result==false){
        this._openSignInDialog();
      }
    });
  }
  public onOpenSignUp():void{
    this._openSignUpDialog()
  }
}
