import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { TitleService } from '../../../services/title.service';

@Component({
  templateUrl: './ride-view.component.html',
  styleUrls: ['./ride-view.component.scss']
})
export class RideViewComponent implements OnInit {
  public getStartedInformation:object = {
    status:'ride',
    title:'_rider_get_started_title',
    backgroundColor:'rgb(18, 28, 39)',
    textColor:'rgb(255,255,255)'
  }
  public howBecomeData:object = {
    status:'ride',
    title:"_key_features_title",
    items:[
      {
        image:'assets/ride/ride-steps/icon1.png',
        title:'_key_features_item_one_title',
        subtitle:'_key_features_item_one_subtitle'
      },
      {
        image:'assets/ride/ride-steps/icon2.png',
        title:'_key_features_item_two_title',
        subtitle:'_key_features_item_two_subtitle'
      },
      {
        image:'assets/ride/ride-steps/icon3.png',
        title:'_key_features_item_three_title',
        subtitle:'_key_features_item_three_subtitle'
      }
    ]
  }
  constructor(private _meta:Meta,private _title:Title,private _titleService:TitleService) { 
      _title.setTitle(_titleService.titlesInCurrentLanguage['howItWorks']) 
  }

  ngOnInit() {
    // this._title.setTitle("Become a Client")
  }

}
