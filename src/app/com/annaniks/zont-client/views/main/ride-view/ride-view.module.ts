import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { RideRoutingModule } from './ride.routing.module';
import { RideViewComponent } from './ride-view.component';
import { RideStepsComponent } from './ride-steps/ride-steps.component';

@NgModule({
    declarations: [     
        RideViewComponent, RideStepsComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RideRoutingModule
    ],
    providers: [
    ]
})
export class RideViewModule { }