import { element } from 'protractor';
import { ActivatedRoute, UrlSegment, Router } from "@angular/router";
import { CitiesService } from "./cities.service";
import { Component, OnInit } from "@angular/core";
import { TranslationsService } from "../../../services/translations.service";

@Component({
  selector: "app-cities",
  templateUrl: "./cities.component.html",
  styleUrls: ["./cities.component.scss"]
})
export class CitiesComponent implements OnInit {
  private _cityId: string;
  private _currentLanguage = window.location.host.split(".");
  private _currentLanguageKey: number = undefined;
  private _cityInformation: any;
  public howBecomeData: object = {
    status: "cities",
    title: "_how_become_zont_cities_title",
    items: [
      {
        image: "assets/cities/1.png",
        title: "_how_become_zont_cities_item_one_title",
        subtitle: "_how_become_zont_cities_item_one_subtitle"
      },
      {
        image: "assets/cities/2.png",
        title: "_how_become_zont_cities_item_two_title",
        subtitle: "_how_become_zont_cities_item_two_subtitle"
      },
      {
        image: "assets/cities/3.png",
        title: "_how_become_zont_cities_item_three_title",
        subtitle: "_how_become_zont_cities_item_three_subtitle"
      }
    ]
  };
  constructor(
    private _citiesService: CitiesService,
    private _activatedRoute: ActivatedRoute,
    private _translationsService: TranslationsService,
    private _router: Router
  ) {}

  ngOnInit() {
    this._getCityData();
  }
  private _getCityData(): void {
    this._translationsService.getLanguages().subscribe((data: any) => {
      data.filter((language: object, index: number) => {
        if (
          language["languageKey"].toLowerCase() ==
          this._currentLanguage[0].toLowerCase()
        ) {
          this._currentLanguageKey = language["id"];
        }
      });
      if (!this._currentLanguageKey) {
        data.forEach((language:any) =>{
          if(language.languageKey == "EN"){
            this._currentLanguageKey =  language["id"];
          }
        })
      }
      this._activatedRoute.url.subscribe((urlSegment: UrlSegment[]) => {
        this._cityId = urlSegment[1].path; 
        this._citiesService
          .getCountriesWithCities(urlSegment[0].path, this._currentLanguageKey)
          .subscribe(countryWithCities => {
            this._cityInformation = this._returnCityInformationByCountry(countryWithCities);
        });
      });
    });
  }
  private _returnCityInformationByCountry(country){
    var cityWasfound:boolean = false;
    for (let index = 0; index < country.cities.length; index++) {
      const element = country.cities[index];
      if (element.city.id == this._cityId) {
        cityWasfound = true
        return element
      }
    }
    if (!cityWasfound) {
      this._router.navigate(["/"]);
      return;
    }
  }
  get cityId(): string {
    return this._cityId;
  }
  get cityInformation(): any {
    return this._cityInformation;
  }
}
