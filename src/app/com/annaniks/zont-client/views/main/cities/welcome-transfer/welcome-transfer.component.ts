import { Component, OnInit } from '@angular/core';

const leftDescription = [
    {
        text: "_zont_description_item_1",
    },
    {
        text: "_zont_description_item_2",
    },
    {
        text: "_zont_description_item_3",
    },
    {
        text: "_zont_description_item_4",
    }
]
const rightDescription = [
    {
        text: "_zont_description_item_5",
    },
    {
        text: "_zont_description_item_6",
    },
    {
        text: "_zont_description_item_7",
    },
    {
        text: "_zont_description_item_8",
    }
]
@Component({
  selector: 'welcome-transfer',
  templateUrl: './welcome-transfer.component.html',
  styleUrls: ['./welcome-transfer.component.scss']
})
export class WelcomeTransferComponent implements OnInit {
 public leftDescription = leftDescription;
 public rightDescription = rightDescription;
  constructor() { }

  ngOnInit() {
  }

}