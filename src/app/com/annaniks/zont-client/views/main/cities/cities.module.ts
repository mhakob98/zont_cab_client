import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './../../../material/material.module';
import { SharedModule } from '../../../shared/shared.module';
import { CitiesComponent } from './cities.component';
import { CitiesRoutingModule } from './cities.routing.module';
import { CitiesService } from './cities.service';
import { WelcomeTransferComponent } from './welcome-transfer/welcome-transfer.component';
import { BestWayToArriveComponent } from './best-way-to-arrive/best-way-to-arrive.component';
import { AuctionsModule } from '../../../auction/auction.module';
import { AirportTransfersComponent } from './airport-transfers/airport-transfers.component';
import { StartTripToComponent } from './start-trip-to/start-trip-to.component';



@NgModule({
    declarations: [
        CitiesComponent,
        WelcomeTransferComponent,
        BestWayToArriveComponent,
        AirportTransfersComponent,
        StartTripToComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        MaterialModule,
        CitiesRoutingModule,
        AuctionsModule
    ],
    providers: [
        CitiesService
    ]
})
export class CitiesModule { }