import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'airport-transfers',
  templateUrl: './airport-transfers.component.html',
  styleUrls: ['./airport-transfers.component.scss']
})
export class AirportTransfersComponent implements OnInit {
  @Input() informationAboutCity;

  constructor() { }

  ngOnInit() {
  }

}
