import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CitiesComponent } from './cities.component';

let citiesRoutes: Routes = [
    // {path:'',component:CitiesComponent},
    // {path:':id',component:CitiesComponent},
    {path:':id/:id',component:CitiesComponent}
]

@NgModule({
    imports: [RouterModule.forChild(citiesRoutes)],
    exports: [RouterModule]
})
export class CitiesRoutingModule { }