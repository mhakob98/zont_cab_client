import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'best-way-to-arrive',
  templateUrl: './best-way-to-arrive.component.html',
  styleUrls: ['./best-way-to-arrive.component.scss']
})
export class BestWayToArriveComponent implements OnInit {
  @Input() informationAboutCity;
  constructor() { }

  ngOnInit() {

  }
  ngAfterViewInit(): void {
  }

}