import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'start-trip-to',
  templateUrl: './start-trip-to.component.html',
  styleUrls: ['./start-trip-to.component.scss']
})
export class StartTripToComponent implements OnInit {
  @Input() informationAboutCity;
  constructor() { }

  ngOnInit() {
  }

}
