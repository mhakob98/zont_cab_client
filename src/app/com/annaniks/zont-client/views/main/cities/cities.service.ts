import { Observable } from "rxjs";
import { ApiService } from "./../../../services/api.service";
import { Injectable } from "@angular/core";

@Injectable()
export class CitiesService {


  constructor(
    private _apiService: ApiService,
  ) {  
  }
  public getCountriesWithCities(countryId: string,langugeKey:number): Observable<any> {
    return this._apiService.get(
      `/country/${countryId}/${langugeKey}`
    );
  }
}
