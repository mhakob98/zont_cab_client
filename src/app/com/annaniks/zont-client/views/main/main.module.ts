import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main.routing.module';
import { MaterialModule } from './../../material/material.module';
import { MainComponent } from './main.component';
import { BottomNavigationBarComponent } from './../../components/bottom-navigation-bar/bottom-navigation-bar.component';
import { TopNavigationBarComponent } from './../../components/top-navigation-bar/top-navigation-bar.component';
import { SignInModal } from '../../modals/sign-in/sign-in.modal';
import { SignUpModal } from '../../modals/sign-up/sign-up.modal';
import { PartnerSignUpModal } from '../../modals/partner-sign-up/partner-sign-up.modal';
import { PaymentService } from '../../services/payment.service';
import {
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
  AuthService,
  SocialLoginModule,
} from "angularx-social-login";
import { SignUpService } from '../../modals/sign-up/sign-up.service';
import { CommonActionsService } from '../../services/common-actions.service';
import { ConfirmModal } from '../../modals/confirm-modal/confirm-modal';
import { ConfirmService } from '../../services/confirm.service';
let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("549571475008-6h8o014lk4kcmjajeug8lmvhjr6aqgdh.apps.googleusercontent.com")
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("882271781934360")
  }
]);
export function provideConfig() {
  return config;
}
@NgModule({
  declarations: [
    MainComponent,
    TopNavigationBarComponent,
    BottomNavigationBarComponent,
    SignInModal,
    SignUpModal,
    PartnerSignUpModal,
    ConfirmModal
  ],
  imports: [
    MainRoutingModule,
    CommonModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    SocialLoginModule
  ],
  providers: [
    AuthService,
    PaymentService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    SignUpService,
    CommonActionsService,
    ConfirmService
  ],
  entryComponents: [SignInModal, SignUpModal, PartnerSignUpModal, ConfirmModal]
})
export class MainModule { }