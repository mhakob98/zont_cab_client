import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DriveViewComponent } from './drive-view.component';

let driveRoutes: Routes = [
    {path:'',component:DriveViewComponent}
]

@NgModule({
    imports: [RouterModule.forChild(driveRoutes)],
    exports: [RouterModule]
})
export class DriveRoutingModule {}