import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { TitleService } from './../../../services/title.service';

@Component({
  selector: 'app-drive-view',
  templateUrl: './drive-view.component.html',
  styleUrls: ['./drive-view.component.scss'],
})
export class DriveViewComponent implements OnInit {
  public getStartedInformation:object = {
    status:'drive',
    title:'_driver_get_started_title',
    backgroundColor:'rgb(255,255,255)',
    textColor:'rgb(18,28,39)'
  }
  public howBecomeData:object = {
    status:'drive',
    title:'_how_become_zont_partner_title',
    items:[{
      image:"assets/drive/how-become-partner/punctual.png",
      title:"_how_become_zont_partner_item_one_title",
      subtitle:"_how_become_zont_partner_item_one_subtitle"
    },
    {
      image:"assets/drive/how-become-partner/clean.png",
      title:"_how_become_zont_partner_item_two_title",
      subtitle:"_how_become_zont_partner_item_two_subtitle"
    },
    {
      image:"assets/drive/how-become-partner/positive.png",
      title:"_how_become_zont_partner_item_three_title",
      subtitle:"_how_become_zont_partner_item_three_subtitle"
    }]
  }
  constructor(private _meta:Meta,private _title:Title,private _titleService:TitleService) {
    setTimeout(() => {
      this._title.setTitle(_titleService.titlesInCurrentLanguage["becomeDriver"])     
    });
   }

   ngOnInit(){

   }
}
