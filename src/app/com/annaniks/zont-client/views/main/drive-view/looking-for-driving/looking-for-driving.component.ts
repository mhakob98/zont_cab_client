import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SignInModal } from '../../../../modals/sign-in/sign-in.modal';
import { SignUpModal } from '../../../../modals/sign-up/sign-up.modal';

@Component({
  selector: 'looking-for-driving',
  templateUrl: './looking-for-driving.component.html',
  styleUrls: ['./looking-for-driving.component.scss'],
})
export class LookingForDrivingComponent implements OnInit {

  constructor(public dialog: MatDialog) {}
  
  ngOnInit(){

  }
  private _openSignInDialog(): void {    
    const dialogRef = this.dialog.open(SignInModal, {
      width: '1100px',
      panelClass:'sign_in_modal'
    });
    dialogRef.afterClosed().subscribe(result => {

    });
   
  }
  private _openSignUpDialog(): void {
    const dialogRef = this.dialog.open(SignUpModal,{
      panelClass:'sign_up_modal'
    })
    dialogRef.afterClosed().subscribe(result => {
      if(result==false){
        this._openSignInDialog();
      }
    });
  }
  public onOpenSignUp():void{
    this._openSignUpDialog()
  }
}
