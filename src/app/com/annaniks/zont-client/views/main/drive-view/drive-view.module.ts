import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { DriveRoutingModule } from './drive-view-routing.module';
import { DriveViewComponent } from "./drive-view.component";
import { LookingForDrivingComponent } from './looking-for-driving/looking-for-driving.component';
import { WhyBecomeDriverComponent } from './why-become-driver/why-become-driver.component';
import { ZontIncomesComponent } from './zont-incomes/zont-incomes.component';
import { SharedModule } from "../../../shared/shared.module";

@NgModule({
    declarations:[DriveViewComponent, LookingForDrivingComponent, WhyBecomeDriverComponent, ZontIncomesComponent],
    imports:[DriveRoutingModule,CommonModule,SharedModule],
    exports:[]
})
export class DriveViewModule{}