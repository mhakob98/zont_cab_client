import { AuthGuard } from './../../services/authguard.service';
import { MainComponent } from './main.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountGuard } from '../../services/accountGuard.service';

let mainRoutes: Routes = [
    {
        path: '', component: MainComponent, children: [
           { path: '', loadChildren: 'src/app/com/annaniks/zont-client/views/main/initial-view/initial-view.module#InitialModule' },
           { path:'become-driver', loadChildren:'src/app/com/annaniks/zont-client/views/main/drive-view/drive-view.module#DriveViewModule'},
           { path:'how-it-works', loadChildren:'src/app/com/annaniks/zont-client/views/main/ride-view/ride-view.module#RideViewModule'},
           { path:'hotels-b2b-tours-operators', loadChildren:'src/app/com/annaniks/zont-client/views/main/partner-view/partner-view.module#PartnerViewModule'},
           { path:'auction', loadChildren:'src/app/com/annaniks/zont-client/views/main/auction-view/auction-view.module#AuctionModule'},
           { path:'cities', loadChildren:'src/app/com/annaniks/zont-client/views/main/cities/cities.module#CitiesModule'},
           { path:'about-us', loadChildren:'src/app/com/annaniks/zont-client/views/main/about-us/about-us.module#AboutUsModule'},
           { path:'rides', loadChildren:'src/app/com/annaniks/zont-client/views/main/rides/rides.module#RidesModule',canActivate:[AccountGuard]},
           { path:'personal-area',loadChildren:'src/app/com/annaniks/zont-client/views/main/personal-area/personal-area.module#PersonalAreaModule',canActivate:[AccountGuard]},
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(mainRoutes)],
    exports: [RouterModule]
})
export class MainRoutingModule { }