import { RidesComponent } from './rides.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

let ridesRoutes: Routes = [
    {path:'',component:RidesComponent}
]

@NgModule({
    imports: [RouterModule.forChild(ridesRoutes)],
    exports: [RouterModule]
})
export class RidesRoutingModule { }