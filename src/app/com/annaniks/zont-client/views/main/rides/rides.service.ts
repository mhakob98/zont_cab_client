import { Observable } from 'rxjs';
import { ApiService } from './../../../services/api.service';
import { Injectable } from "@angular/core";
import { Preorder } from '../../../models/models';

@Injectable()
export class RidesService{
    constructor(private _apiService:ApiService){}

    public getAllAuctionsHistory(pageNumber:number,count:number,startDate:string,endDate:string,type:string):Observable<Preorder[]>{
        return this._apiService.get(`/Auction/client/auctions?type=new&pageNumber=${pageNumber}&count=${count}&startDate=${startDate}&endDate=${endDate}&type=${type}&isDescending=true`,true)    
    }
    public getAuctionsCount():Observable<number>{
        return this._apiService.get("/Auction/client/auctions/count",true);
    }
    public getTripsCount():Observable<number>{       
        return this._apiService.get("/Trip/client/count",true)
    }
    public getAllTrips(pageIndex:number,pageSize:number){
        return this._apiService.get(`/Trip/client?type=new&pageNumber=${pageIndex}&count=${pageSize}$=isDescending=false`,true)
    }
}