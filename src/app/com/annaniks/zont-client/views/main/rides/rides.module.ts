import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './../../../material/material.module';
import { RidesRoutingModule } from './rides.routing.module';
import { SharedModule } from '../../../shared/shared.module';
import { RidesComponent } from './rides.component';
import { RidesService } from './rides.service';


@NgModule({
    declarations: [     
        RidesComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        MaterialModule,
        RidesRoutingModule
    ],
    providers: [
        RidesService
    ]
})
export class RidesModule { }