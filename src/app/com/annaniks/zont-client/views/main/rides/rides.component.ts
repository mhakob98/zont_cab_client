import { RidesService } from './rides.service';
import { Component, OnInit } from '@angular/core';
import { Preorder } from '../../../models/models';

@Component({
  templateUrl: './rides.component.html',
  styleUrls: ['./rides.component.scss']
})
export class RidesComponent implements OnInit {
  private _allTrips:any = [];
  private _allTripsCount:number = 0;
  private _allPreorders:Array<Preorder> = [];
  private _allPreordersCount:number;
  private _page:number = 1;
  private _startDate:string;
  private _endDate:string;
  constructor(private _ridesService:RidesService ) {}
  ngOnInit() {
    this._getAllTripsCount();
    this._getAllTrips(1,10);
    this._getAllPreOrders(0,10,"","","oldestNewest");
    this._getAllPreOrdersCount()
  }
  public handlePreordersPaginate(event){
    this._getAllPreOrders(event.pageNumber,10,this.startDate,this.endDate,"oldestNewest")
  }
  public handleBasicTripsPaginate(event){
    this._getAllTrips(event.pageNumber,10)
  }
  public _getAllPreOrders(pageNumber:number,count:number,startDate:string,endDate:string,type:string):void{
    this._ridesService.getAllAuctionsHistory(pageNumber,count,startDate,endDate,type).subscribe((allPreorders:Preorder[])=>{
      this._allPreorders = allPreorders
    })
  }
  private _getAllPreOrdersCount():void{
    this._ridesService.getAuctionsCount().subscribe((countOfAllPreOrders:number)=>{
      this._allPreordersCount = countOfAllPreOrders;
    })
  }
  public _getAllTrips(pageNumber:number,count:number):void{
    this._ridesService.getAllTrips(pageNumber,count).subscribe((allTrips:any)=>{
      
      this._allTrips = allTrips
    })
  }
  private _getAllTripsCount():void{
    this._ridesService.getTripsCount().subscribe((countOfAllTrips:number)=>{
      this._allTripsCount = countOfAllTrips;
    })
  }
  public handleSearchEvent(timeZone:object){
    this.startDate = timeZone['startDate'];
    this.endDate = timeZone['endDate'];
    this._getAllPreOrders(1,10,this.startDate,this.endDate,"oldestNewest")

  }
  get allTripsCount():number{
    return this._allTripsCount
  }
  get allTrips():any[]{
    return this._allTrips
  }
  get allPreordersCount():number{
    return this._allPreordersCount
  }
  get allPreorders():Preorder[]{
    return this._allPreorders
  }
  get page():number{
    return this._page;
  }
  set startDate(startDate:string) {
    this._startDate = startDate;
  }
  get startDate() : string {
    return this._startDate
  }
  set endDate(endDate : string) {
    this._endDate = endDate;
  }
  get endDate() : string {
    return this._endDate
  }

}
