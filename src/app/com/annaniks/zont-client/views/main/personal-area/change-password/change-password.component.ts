import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Component, OnInit, Inject } from "@angular/core";
import { PersonalAreaService } from "../personal-area.service";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "change-password",
  templateUrl: "./change-password.component.html",
  styleUrls: ["./change-password.component.scss"]
})
export class ChangePasswordComponent implements OnInit {
  private _changePasswordForm: FormGroup;
  private _showOverlay:boolean;
  constructor(@Inject(FormBuilder) private _formBuilderInstance: FormBuilder,private _personalAreaService:PersonalAreaService,private _toastr: ToastrService) {}

  ngOnInit() {
    this._changePasswordFormBuilder();
  }
  private _changePasswordFormBuilder(): void {
    this._changePasswordForm = this._formBuilderInstance.group({
      oldPassword: [null, [Validators.required, Validators.minLength(6)]],
      newPassword: [null, [Validators.required, Validators.minLength(6)]]
    });
  }
  public onChangePassword():void{
    this._changePasswordForm.valid ? this._changePassword(): null
  }
  private _changePassword():void{
    this._showOverlay = true;
    this._personalAreaService.changePassword(this._changePasswordForm.value).subscribe(data=>{
        this._changePasswordSuccessMessage("Your password has been successfully changed")
    },error => this._changePasswordFailMessage("Old Passsword is invalid"))
  }
  private _changePasswordFailMessage(errorMessage:string):void{
    this._toastr.error('',errorMessage,{
      positionClass:'toast-top-center'
    })
    this._showOverlay = false;
    this._changePasswordForm.reset()

  }
  private _changePasswordSuccessMessage(succesMessage:string):void{
    this._toastr.success('',succesMessage,{
      positionClass:'toast-top-center'
    })
    this._showOverlay = false;
    this._changePasswordForm.reset()

  }
  get changePasswordForm(): FormGroup {
    return this._changePasswordForm;
  }
  get showOverlay():boolean{
    return this._showOverlay;
  }
}
