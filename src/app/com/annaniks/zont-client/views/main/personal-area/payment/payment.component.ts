import { Component, OnInit, NgZone, ElementRef, ViewChild } from '@angular/core';
import { PaymentService } from './../../../../services/payment.service';
import { CardResponse } from './../../../../models/models';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  @ViewChild("month") monthElement: ElementRef;
  @ViewChild("year") yearElement: ElementRef;
  private _allCards:CardResponse [];
  private _paymentForm: FormGroup;
  private _addNewCard:boolean = false;
  private _showOverlay:boolean = false;
  constructor(private _paymentService:PaymentService,private _fb: FormBuilder,private _zone: NgZone,private _toastr: ToastrService) { }
  
  ngOnInit() {
    this._getAllBankCard();
    this._formBuilder();
  }
  private _getAllBankCard():void{
    this._paymentService.getCards().subscribe((allBankCards:CardResponse[])=>{
      this._allCards = allBankCards;
    })
  }
  private _formBuilder(): void {
    this._paymentForm = this._fb.group({
      cardNumber: ["", [Validators.required, Validators.minLength(16)]],
      cardHolderName: ["", Validators.required],
      expDateMonth: ["", [Validators.required,Validators.maxLength(2),Validators.minLength(2)]],
      expDateYear:["",[Validators.required,Validators.maxLength(2),Validators.minLength(2)]],
      cvv: ["", Validators.required]
    });
    this._paymentForm.get('expDateMonth').valueChanges.subscribe((value)=>{
      if( value && value.length >= 2){
        this.yearElement.nativeElement.focus()
      }
    })
  }
  public toggleAddCardValue(add?:boolean):void{
    if(add){
      this._createToken() 
    } else{
      this._paymentForm.reset();
      this._addNewCard =! this._addNewCard
    }
  }
  private _createToken():void{
    this._showOverlay = true;
    (<any>window).Stripe.card.createToken(
      {
        number: this._paymentForm.value["cardNumber"],
        exp_month: this._paymentForm.value["expDateMonth"],
        exp_year: this._paymentForm.value["expDateYear"],
        cvc: this._paymentForm.value["cvv"]
      },
      (status: number, response: any) => {
        this._zone.run(() => {
          if (status === 200) {
            
            this._addCard(response.id);
          } else {
            this._signInFailMessage("You entered incorrect data or we do not support your card")
            this._showOverlay = false;
          }
        });
      }
    );
  }
  private _addCard(cardId:string):void{
    this._paymentService.addCard(cardId).subscribe((response:any)=>{
        this._getAllCards()
        this._showOverlay = false;
    })
  }
  private _getAllCards():void{
    this._paymentService.getCards().subscribe((cards:CardResponse[])=>{
      this._allCards = cards;
      this._addNewCard = false;
      this._showOverlay = false;
    })
  }
  private _signInFailMessage(errorMessage:string):void{
    this._toastr.error('',errorMessage,{
      positionClass:'toast-top-center'
    })
  }
  public handleCardDeleteEvent():void{
    this._getAllBankCard();
  }
  public getValue():void{

  }
  get allCards():CardResponse[]{
    return this._allCards;
  }
  get paymentForm(): FormGroup {
    return this._paymentForm;
  }
  get addNewCard():boolean{
    return this._addNewCard;
  }
  get showOverlay():boolean{
    return this._showOverlay;
  }
}
