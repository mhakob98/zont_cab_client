import { ScrollService } from './../scroll.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'personal-area-controls',
  templateUrl: './personal-area-controls.component.html',
  styleUrls: ['./personal-area-controls.component.scss']
})
export class PersonalAreaControlsComponent implements OnInit {
  constructor(private _scrollService:ScrollService) { }

  ngOnInit() {
  }
  public scrollTo(destination:string):void{
    this._scrollService.triggerScrollTo(destination)
  }
  get scrollService():ScrollService{
    return this._scrollService
  }

}
