import { PersonalArea } from './personal-area';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

let personalAreaRoutes: Routes = [
    {path:'',component:PersonalArea}
]

@NgModule({
    imports: [RouterModule.forChild(personalAreaRoutes)],
    exports: [RouterModule]
})
export class PersonalAreaRoutingModule { }