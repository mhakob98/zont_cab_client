import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
import { Injectable } from '@angular/core';
@Injectable()
export class ScrollService {
 
    constructor(private _scrollToService: ScrollToService) { }
   
    private _activeLink:string = "contactDetail"
    
    public triggerScrollTo(destination:string) {
      this._activeLink = destination;
      const config: ScrollToConfigOptions = {
        target: destination
      };

      this._scrollToService.scrollTo(config);
    }
    get activeLink():string{
      return this._activeLink;
    }
    set activeLink(link : string) {
      this._activeLink = link;
    }
    
  }