import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'personal-area-header',
  templateUrl: './personal-area-header.component.html',
  styleUrls: ['./personal-area-header.component.scss']
})
export class PersonalAreaHeaderComponent implements OnInit {
  @Input() client:any;
  constructor() { }

  ngOnInit() {
  }

}
