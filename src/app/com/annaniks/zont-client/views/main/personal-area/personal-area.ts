import { Title, Meta } from '@angular/platform-browser';
import { ScrollService } from './scroll.service';
import { Component, OnInit } from '@angular/core';
import { PersonalAreaService } from './personal-area.service';

@Component({
  selector: 'app-personal-area',
  templateUrl: './personal-area.html',
  styleUrls: ['./personal-area.scss']
})
export class PersonalArea implements OnInit {
  private _client:any;
  constructor(private _personalAreaService:PersonalAreaService,private _scrollService:ScrollService,private _title:Title,private _meta:Meta) { }

  ngOnInit() {
    this._getClient();
  }
  private _getClient():void{
    this._personalAreaService.getClient().subscribe((client:any)=>{
      this._client = client;    
    })
  }
  get client():any{
    return this._client;
  }
  public handleUpdateClientInfo(){
    this._getClient();
  }
  public onIntersection(event,destination):void{      
    // this._scrollService.activeLink = destination
  }

 
}
