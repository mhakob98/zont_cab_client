import { Observable } from 'rxjs';
import { ApiService } from './../../../services/api.service';
import { Injectable } from "@angular/core";

@Injectable()
export class PersonalAreaService{
    constructor(private _apiService:ApiService){}
    public changePassword(passwordData):Observable<null>{
        return this._apiService.post(`/client/changePassword`,passwordData)
    }
    public getClient():Observable<any>{
        return this._apiService.get(`/client`,true);
    }
    public editClient(clientForm):Observable<any>{
        return this._apiService.post('/client/edit',clientForm)
    }
}