import { Component, OnInit, Input, Inject,EventEmitter, Output } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { PersonalAreaService } from "../personal-area.service";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "contact-detail",
  templateUrl: "./contact-detail.component.html",
  styleUrls: ["./contact-detail.component.scss"]
})
export class ContactDetailComponent implements OnInit {
  @Input() client:any;
  @Output() updateClientInfo:EventEmitter<void> = new EventEmitter<void>()
  private _contactDetailForm: FormGroup;
  private _showOverlay:boolean;
  constructor(@Inject(FormBuilder) private _formBuilderInstance: FormBuilder,private _personalAreaService:PersonalAreaService,private _toastr: ToastrService) {}

  ngOnInit() {      
      this._updateContactDetail();
  }

  private _updateContactDetail(): void {
    this._contactDetailForm = this._formBuilderInstance.group({
      firstName: [this.client.firstName, [Validators.required, Validators.minLength(3)]],
      lastName: [this.client.lastName, [Validators.required, Validators.minLength(3)]],
      dateOfBirth:[this.client.dateOfBirth,[Validators.required,Validators.pattern(/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/)]],
      email: [{value:this.client.email,disabled: true},[Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
    });
  }
  public onEditClientAccount():void{
    this._contactDetailForm.valid ? this._editClientAccount() : null;
  }
  private _editClientAccount():void{
    this._showOverlay = true
    var clientEditForm = Object.assign({}, this._contactDetailForm.value, { phoneNumber: "+37498783092", bankCard: null, id: this.client.id });    
    this._personalAreaService.editClient(clientEditForm).subscribe((editedClientData: any) => {
        this._showOverlay = false;
        this.updateClientInfo.emit();        
    })
  }
  get contactDetailForm():FormGroup{
    return this._contactDetailForm;
  }
  get showOverlay():boolean{
    return this._showOverlay;
  }
}
