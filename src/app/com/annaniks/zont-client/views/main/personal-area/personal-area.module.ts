import { PersonalAreaRoutingModule } from './personal-area.routing.module';
import { PersonalArea } from './personal-area';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './../../../material/material.module';
import { SharedModule } from '../../../shared/shared.module';
import { PersonalAreaService } from './personal-area.service';
import { PersonalAreaHeaderComponent } from './personal-area-header/personal-area-header.component';
import { PersonalAreaControlsComponent } from './personal-area-controls/personal-area-controls.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { PaymentComponent } from './payment/payment.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { ScrollService } from './scroll.service';

@NgModule({
    declarations: [     
       PersonalArea, PersonalAreaHeaderComponent, PersonalAreaControlsComponent, ContactDetailComponent, ChangePasswordComponent, PaymentComponent, NotificationsComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        MaterialModule,
        PersonalAreaRoutingModule
    ],
    providers: [
        PersonalAreaService,
        ScrollService
    ]
})
export class PersonalAreaModule { }