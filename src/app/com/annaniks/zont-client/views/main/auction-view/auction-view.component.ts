import { AuctionService } from './../../../auction/auction.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SignInModal } from '../../../modals/sign-in/sign-in.modal';
import { ToastrService } from 'ngx-toastr';
import { Auction, Trip, MakeAuction } from '../../../models/models';
import { CookieService } from 'ngx-cookie-service';
import * as _moment from 'moment';
import { AuctionViewService } from './auction-view.service';
import { takeWhile } from 'rxjs/operators';

const moment = _moment;
@Component({
  selector: 'app-auction-view',
  templateUrl: './auction-view.component.html',
  styleUrls: ['./auction-view.component.scss']
})
export class AuctionViewComponent implements OnInit, OnDestroy {
  private _selectedTripType: Trip;
  private _finalAuctioninfo: MakeAuction;
  private _showOverlay: boolean = false;
  public subscribe: boolean = true;
  constructor(
    private _router: Router,
    private _auctionsService: AuctionService,
    private _cookieService: CookieService,
    public _dialog: MatDialog,
    private _toastr: ToastrService,
    private _auctionViewService: AuctionViewService
  ) { }

  ngOnInit() {
    this._auctionViewService.taxiOrderedState.pipe(takeWhile(() => this.subscribe)).subscribe(cardId => this.bookTaxi(cardId));
    this._auctionViewService.auctionData = this._router.getNavigatedData()
    if (this._auctionViewService.auctionData == undefined) {
      this._router.navigate([''])
    }
  }
  public handleSelectedTripType(selectedTripType: Trip): void {
    this._selectedTripType = selectedTripType
  }
  public bookTaxi(cardId: string): void {
    let auctionInfo = this._auctionViewService.auctionData
    this._finalAuctioninfo = {
      startPointLatitude: auctionInfo.pickupPointLatitude,
      startPointLongitude: auctionInfo.pickupPointLongitude,
      startDate: moment(`${auctionInfo.date} ${auctionInfo.time}:00`, "DD/MM/YYYY HH:mm:ss").utc().format("DD/MM/YYYY HH:mm:ss"),
      duration: auctionInfo.hours * 60 * 60,
      destination: auctionInfo.hours ? null : auctionInfo.destination,
      clientPrice: auctionInfo.hours ? (this._selectedTripType.perMinuteForTime * auctionInfo.hours * 60) + this._selectedTripType.baseFare : this._selectedTripType.maxAmount,
      tripType: auctionInfo.hours ? "timing" : "distance",
      carType: auctionInfo.hours ? this._selectedTripType.tripTypes : this._selectedTripType.tripType,
      distance: this._selectedTripType.distance,
      terminal: "Some Comment",
      cardId: cardId,
    }
    if (this._cookieService.get("Client_accessToken")) {
      this._showOverlay = true;
      this._auctionsService.makeAuction(this._finalAuctioninfo).subscribe(response => {
        this._showOverlay = false;
        this._auctionSuccessMessage()
        setTimeout(() => {
          this._router.navigate(['/rides']);
        }, 3000);
      }, error => {
        this._showOverlay = false;
        this._signInFailMessage("Something went wrong")
      })
    } else {
      this._openSignInDialog();
    }
  }
  private _openSignInDialog(): void {
    const dialogRef = this._dialog.open(SignInModal, {
      width: '1100px',
      panelClass: 'sign_in_modal',
      data: {
        status: 'auction',
        tripInfo: this._finalAuctioninfo,
        auctionInfo: this._auctionViewService.auctionData
      }
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  private _auctionSuccessMessage(): void {
    this._toastr.success('', "Your order is confirmed", {
      positionClass: 'toast-top-center',
      timeOut: 3000,
    })
  }
  private _signInFailMessage(errorMessage: string): void {
    this._toastr.error('', errorMessage, {
      positionClass: 'toast-top-center'
    })
  }
  get showOverlay(): boolean {
    return this._showOverlay
  }
  ngOnDestroy() {
    this.subscribe = false;
  }
}
