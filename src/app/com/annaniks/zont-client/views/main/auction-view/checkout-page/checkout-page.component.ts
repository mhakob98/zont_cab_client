import { CardResponse, Auction } from './../../../../models/models';
import { PaymentService } from './../../../../services/payment.service';
import { Component, OnInit, NgZone, Output, EventEmitter, Input, ViewChild, ElementRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { AuctionViewService } from '../auction-view.service';
declare var google: any;
declare var MarkerWithLabel: any

@Component({
  selector: "checkout-page",
  templateUrl: "./checkout-page.component.html",
  styleUrls: ["./checkout-page.component.scss"],
})
export class CheckoutPageComponent implements OnInit {
  // Components communication operators
  @Input()
  selectedTripType: any

  //Child elements 
  @ViewChild("month")
  monthElement: ElementRef;
  @ViewChild("year")
  yearElement: ElementRef;


  // Variables
  auctionInfo: Auction;
  paymentForm: FormGroup;
  isBookingButtonAccessable: boolean = true;
  voucherForm: FormGroup;
  cardId: string;
  showOverlay: boolean = false;
  addNewCard: boolean = false;
  map: any;
  voucherCode: boolean = false;
  voucherCodeValue: string = "";
  allCards: CardResponse[];
  value: number = 0;
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
  public icons = {
    start: {
      url: "assets/map/start.png", // url
      scaledSize: new google.maps.Size(30, 30), // scaled size
      origin: new google.maps.Point(0, 0), // origin
      anchor: new google.maps.Point(0, 0) // anchor
    },
    end: {
      url: 'assets/map/end.png', // url
      scaledSize: new google.maps.Size(30, 35), // scaled size
      origin: new google.maps.Point(0, 0), // origin
      anchor: new google.maps.Point(0, 0) // anchor
    }
  };

  constructor(
    private _fb: FormBuilder,
    private _zone: NgZone,
    private _paymentService: PaymentService,
    private _toastr: ToastrService,
    private _cookieService: CookieService,
    private _auctionViewService: AuctionViewService
  ) { }

  ngOnInit() {
    this.auctionInfo = this._auctionViewService.auctionData
    this._formBuilder();
    this._initMap();
    this._getAllCards()
  }
  private _initMap(corrdinates = { lat: 40.7865229, lng: 43.8476395 }, zoom = 15): void {
    var uluru = corrdinates;
    this.map = new google.maps.Map(document.getElementById("map"), {
      zoom: zoom,
      center: uluru,
      disableDefaultUI: true
    });
    this.directionsDisplay.setMap(this.map);
    this.calcRoute();
  }
  public handleCardDeleteEvent(): void {
    this._getAllCards();
  }
  private _formBuilder(): void {
    this.paymentForm = this._fb.group({
      cardNumber: ["", [Validators.required, Validators.minLength(16)]],
      cardHolderName: ["", Validators.required],
      expDateMonth: ["", [Validators.required, Validators.maxLength(2), Validators.minLength(2), Validators.maxLength(2)]],
      expDateYear: ["", [Validators.required, Validators.maxLength(2), Validators.minLength(2), Validators.maxLength(2)]],
      cvv: ["", Validators.required]
    });
    this.voucherForm = this._fb.group({
      voucherCode: ["", Validators.required]
    });
    this.paymentForm.get('expDateMonth').valueChanges.subscribe((value) => {
      if (value && value.length >= 2) {
        this.yearElement.nativeElement.focus()
      }
    })
  }
  public bookTaxi(): void {
    this.isBookingButtonAccessable = false;
    this._createToken(false)
  }
  public toggleAddCardValue(add?: boolean): void {
    if (add) {
      this._createToken(true)
    } else {
      this.paymentForm.enable();
      this.paymentForm.reset();
      this.addNewCard = !this.addNewCard
    }
  }
  public checkIfBookingIsAccesable(): boolean {
    if (this.isBookingButtonAccessable == false) {
      return true;
    }
    return this._cookieService.get("Client_accessToken") &&
      this.isBookingButtonAccessable &&
      (this.cardId || (this.allCards == undefined || this.allCards.length < 1))
      ? false : true
  }
  public clickApply(): void {
    this.voucherCode = !this.voucherCode;
    this.voucherCodeValue = this.voucherForm.value.voucherCode
    this.voucherForm.reset()
  }
  private _addCard(cardId: string, addSerialCard: boolean): void {
    this._paymentService.addCard(cardId).subscribe((response: any) => {
      if (addSerialCard) {
        this._getAllCards()
      } else {
        this._auctionViewService.taxiOrdered.next(response.body);
        this.showOverlay = false;
      }
    })
  }
  private _getAllCards(): void {
    this._paymentService.getCards().subscribe((cards: CardResponse[]) => {
      this.allCards = cards;
      if (this.allCards.length > 0) {
        this.paymentForm.disable()
      }
      this.addNewCard = false;
      this.showOverlay = false;
    }, error => {
      this.showOverlay = false;
      this.allCards = [];
    })
  }
  private _signInFailMessage(errorMessage: string): void {
    this._toastr.error('', errorMessage, {
      positionClass: 'toast-top-center'
    })
  }
  private _createToken(addSerialCard: boolean): void {
    this.showOverlay = true;
    if (this.cardId && !this.addNewCard) {
      this._auctionViewService.taxiOrdered.next(this.cardId);
      this.showOverlay = false;

      return
    }
    (<any>window).Stripe.card.createToken(
      {
        number: this.paymentForm.value["cardNumber"],
        exp_month: this.paymentForm.value["expDateMonth"],
        exp_year: this.paymentForm.value["expDateYear"],
        cvc: this.paymentForm.value["cvv"]
      },
      (status: number, response: any) => {
        this._zone.run(() => {
          if (status === 200) {
            this._addCard(response.id, addSerialCard);
          } else {
            this._signInFailMessage("You entered incorrect data or we do not support your card")
            this.showOverlay = false;
            this.isBookingButtonAccessable = true;
          }
        });
      }
    );
  }
  calcRoute() {
    if (this.auctionInfo.destination) {
      var destinationCoordinates = this.auctionInfo.destination.split(",")
      var origin = new google.maps.LatLng(this.auctionInfo.pickupPointLatitude, this.auctionInfo.pickupPointLongitude);
      var destination = new google.maps.LatLng(destinationCoordinates[0], destinationCoordinates[1]);
      var request = {
        origin: origin,
        destination: destination,
        travelMode: google.maps.TravelMode['DRIVING'],
      };
      this.directionsService.route(request, (response, status) => {
        if (status == 'OK') {
          new google.maps.DirectionsRenderer({
            map: this.map,
            directions: response,
            suppressMarkers: true
          });
          var leg = response.routes[0].legs[0];
          this._makeMarker(leg.start_location, this.icons.start, "title", this.map);
          this._makeMarker(leg.end_location, this.icons.end, 'title', this.map);
        }
      });
    }
  }
  private _makeMarker(position, icon, title, map) {
    new google.maps.Marker({
      position: position,
      map: map,
      title: title,
      animation: google.maps.Animation.DROP,
      icon: icon
    });
  }

  public getValue($event): void {
    this.cardId = $event;
  }


}
