import { Injectable } from "@angular/core";
import { Subject, Observable } from "rxjs";
import { Auction } from "../../../models/models";

@Injectable()
export class AuctionViewService {
    // Observables
    taxiOrdered: Subject<string> = new Subject<string>();
    taxiOrderedState: Observable<string>;

    // Container variables
    auctionData: Auction

    constructor() {
        this.taxiOrderedState = this.taxiOrdered.asObservable();
    }
}