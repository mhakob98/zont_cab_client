import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuctionViewComponent } from './auction-view.component';

let auctionRoutes: Routes = [
    { path: '', component: AuctionViewComponent }
]
@NgModule({
    imports: [RouterModule.forChild(auctionRoutes)],
    exports: [RouterModule]
})
export class AuctionsRoutingModule { }