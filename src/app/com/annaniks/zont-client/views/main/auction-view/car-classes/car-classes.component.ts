import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Auction, Trip } from '../../../../models/models';
import { AuctionViewService } from '../auction-view.service';

const CLASSDESCRIPTIONS: Array<object> = [
  {
    image: "assets/auction/car-classes/clock.png",
    label: '_preorder_advantage_one',
  },
  {
    image: "assets/auction/car-classes/meet.png",
    label: '_preorder_advantage_two',

  },
  {
    image: "assets/auction/car-classes/stop.png",
    label: '_preorder_advantage_three',

  }
]
@Component({
  selector: 'car-classes',
  templateUrl: './car-classes.component.html',
  styleUrls: ['./car-classes.component.scss']
})
export class CarClassesComponent implements OnInit {
  @Output() selectedTripType: EventEmitter<Trip> = new EventEmitter<Trip>();
  @Output() nextTab: EventEmitter<void> = new EventEmitter<void>();
  private _tripTypes: Array<Trip>
  private _classDescriptions = CLASSDESCRIPTIONS;
  private _activeTripType: Trip;
  auctionInfo: Auction;
  constructor(
    private _auctionViewService: AuctionViewService
  ) { }

  ngOnInit() {
    this.auctionInfo = this._auctionViewService.auctionData;
    this._tripTypes = this.auctionInfo.tripInfo;
    this._activeTripType = this.auctionInfo.tripInfo[0];
    this.auctionInfo.tripInfo.forEach(carType => {
      carType.tripType = carType.tripTypes ? carType.tripTypes : carType.tripType;
      if (carType.tripType == this.auctionInfo.carType) {
        this._activeTripType = carType
      }
    });
    this.selectedTripType.emit(this._activeTripType)
  }

  public onCarClassClick(car: Trip): void {
    this._activeTripType = car;
    this.selectedTripType.emit(this._activeTripType);
  }
  public continueTab(): void {
    this.nextTab.emit()
  }
  get tripTypes(): Array<Trip> {
    return this._tripTypes;
  }
  get activeTripType(): Trip {
    return this._activeTripType;
  }
  get classDescriptions(): Array<object> {
    return this._classDescriptions;
  }
}
