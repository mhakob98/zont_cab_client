import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { SharedModule } from "../../../shared/shared.module";
import { AuctionsRoutingModule } from "./auction-view.routing.module";
import { AuctionViewComponent } from "./auction-view.component";
import { AuctionDescriptionComponent } from './auction-description/auction-description.component';
import { AuctionDestinationComponent } from './auction-destination/auction-destination.component';
import { AuctionTabComponent } from './auction-tab/auction-tab.component';
import { CarClassesComponent } from './car-classes/car-classes.component';
import { CheckoutPageComponent } from "./checkout-page/checkout-page.component";
import { AuctionViewService } from "./auction-view.service";

@NgModule({
    declarations: [AuctionViewComponent, AuctionDescriptionComponent, AuctionDestinationComponent, AuctionTabComponent, CarClassesComponent, CheckoutPageComponent],
    imports: [CommonModule, SharedModule, AuctionsRoutingModule],
    providers: [AuctionViewService],
    exports: []
})
export class AuctionModule { }