import { Component, Input, Output, EventEmitter, AfterViewInit, OnInit } from '@angular/core';
import { Auction, Trip } from '../../../../models/models';
import { CookieService } from 'ngx-cookie-service';
import { AuctionViewService } from '../auction-view.service';

const TABLINKS: string[] = ["_preorder_car_class", "_preorder_login", "_preorder_checkout"];
const SIGNINORSIGNUP: string[] = ['signIn', 'signUp']
@Component({
  selector: 'auction-tab',
  templateUrl: './auction-tab.component.html',
  styleUrls: ['./auction-tab.component.scss']
})
export class AuctionTabComponent implements AfterViewInit, OnInit {

  // Components communication operators
  @Output()
  selectedTripType: EventEmitter<Trip> = new EventEmitter<Trip>();
  @Output()
  onNextTabClick: EventEmitter<void> = new EventEmitter<void>();

  // Variables
  auctionInfo: Auction;
  tripType: Trip
  activeLink: string = "_preorder_car_class";
  signInorSignUp: string[] = SIGNINORSIGNUP;
  signInOrSignUpActive: string = "signIn"
  tabLinks = TABLINKS;


  constructor(
    private _cookieService: CookieService,
    private _auctionViewService: AuctionViewService
  ) { }

  ngOnInit() {
    this.auctionInfo = this._auctionViewService.auctionData
  }
  ngAfterViewInit(): void {
    this.tripType = this.auctionInfo.tripInfo[0];
  }
  public onTabLinkClick(link: string): void {
    this.activeLink = link;
  }
  public onSignInOrSignUpTabLinkClick(link: string): void {
    this.signInOrSignUpActive = link;
  }
  public nextTabAfterCarClass(): void {
    if (this._cookieService.get("Client_accessToken")) {
      this.activeLink = "_preorder_checkout"
    } else {
      this.activeLink = "_preorder_login"
    }
  }
  public nextPageAfterLoggedInOrSignUp(): void {
    this.activeLink = "_preorder_checkout";
  }
  public onSelectedTripTypeChange(tripType: Trip): void {
    this.tripType = tripType
    this.selectedTripType.emit(tripType);
  }
  public auctionMakeTransfer(): void {
    this.onNextTabClick.emit()
  }

}
