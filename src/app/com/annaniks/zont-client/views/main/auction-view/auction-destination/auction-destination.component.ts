import { Component, OnInit, Input } from '@angular/core';
import { Auction } from '../../../../models/models';
import { AuctionViewService } from '../auction-view.service';

@Component({
  selector: 'auction-destination',
  templateUrl: './auction-destination.component.html',
  styleUrls: ['./auction-destination.component.scss']
})
export class AuctionDestinationComponent implements OnInit {
  auctionInfo: Auction;
  constructor(
    private _auctionViewService: AuctionViewService
  ) { }

  ngOnInit() {
    this.auctionInfo = this._auctionViewService.auctionData
  }
  public formatDate(date: string): string {
    var finalFormat = ''
    var day = date.split(" ")[0].split("/").reverse();
    day.forEach(element => {
      finalFormat += element + "/"
    });
    return finalFormat
  }
}
