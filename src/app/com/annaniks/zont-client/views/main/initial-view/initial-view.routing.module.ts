import { InitialViewComponent } from './initial-view.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

let initialRoutes: Routes = [
    {path:'',component:InitialViewComponent}
]

@NgModule({
    imports: [RouterModule.forChild(initialRoutes)],
    exports: [RouterModule]
})
export class InitialRoutingModule { }