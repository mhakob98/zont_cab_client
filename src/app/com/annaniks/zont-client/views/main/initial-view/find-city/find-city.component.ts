import { InitialViewServiceService } from './../initial-view-service.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'find-city',
  templateUrl: './find-city.component.html',
  styleUrls: ['./find-city.component.scss'],
  })
export class FindCityComponent implements OnInit {
  public selectedCity:object;
  public cities:Array<{id:number,name:string,routerLink:string}> =[];
  constructor(private _router:Router,private _initialViewServiceService:InitialViewServiceService) { }
  public navigateToCity(city:{id:number,name:string,routerLink:string}):void{    
    this._router.navigate([`${city.routerLink}`]);
  }
  ngOnInit() {
    this._initialViewServiceService._getAllCountries().subscribe(countries=>{
      countries.forEach((element,index) => {
        this.cities = [...this.cities, {
          id:index,
          name:element.city.key,
          routerLink:`/cities/${element.city.country.id}/${element.city.id}`
        }];
      });      
    })
    
  }

}
