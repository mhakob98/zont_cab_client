import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuctionsModule } from './../../../auction/auction.module';
import { InitialViewComponent } from './initial-view.component';
import { InitialRoutingModule } from './initial-view.routing.module';
import { MobileApplicationAdd } from './mobile-application-add/mobile-application-add';
import { FindCityComponent } from './find-city/find-city.component';
import { WhyZontComponent } from './why-zont/why-zont.component';
import { SharedModule } from '../../../shared/shared.module';
import { InitialViewServiceService } from './initial-view-service.service';

@NgModule({
    declarations: [
        InitialViewComponent,
        MobileApplicationAdd,
        FindCityComponent,
        WhyZontComponent
    ],
    imports: [
        InitialRoutingModule,
        CommonModule,
        AuctionsModule,
        SharedModule
    ],
    providers: [
        InitialViewServiceService
    ]
})
export class InitialModule { }