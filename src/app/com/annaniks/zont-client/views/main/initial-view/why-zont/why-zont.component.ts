import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'why-zont',
  templateUrl: './why-zont.component.html',
  styleUrls: ['./why-zont.component.scss'],
})
export class WhyZontComponent implements OnInit {
  public zontAdvantages = [
    {
      image:"assets/initial/why-zont/tranquility.png",
      title:"_why_zont_tranquility",
      subtitle:'_why_zont_tranquility_description'
    },
    {
      image:"assets/initial/why-zont/car-localisation.png",
      title:"_why_zont_car_localisation",
      subtitle:'_why_zont_car_localisation_description'
    },
    {
      image:"assets/initial/why-zont/worldwide-service.png",
      title:"_why_zont_worldwide_service",
      subtitle:'_why_zont_worldwide_service_description'
    },
  ]
  constructor() { }

  ngOnInit() {
  }

}
