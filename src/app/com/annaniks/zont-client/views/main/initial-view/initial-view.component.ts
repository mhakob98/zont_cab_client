import { Component, OnInit } from '@angular/core';
import { InitialViewServiceService } from './initial-view-service.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'initial-view',
  templateUrl: './initial-view.component.html',
  styleUrls: ['./initial-view.component.scss']
})
export class InitialViewComponent implements OnInit {

  constructor(private _initialViewServiceService:InitialViewServiceService,private _title:Title) {
    this._title.setTitle("Zont")
   }

  ngOnInit() {
    
  }

}
