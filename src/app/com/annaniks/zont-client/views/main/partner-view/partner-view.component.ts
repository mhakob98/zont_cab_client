import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { TitleService } from '../../../services/title.service';

@Component({
  selector: 'app-partner-view',
  templateUrl: './partner-view.component.html',
  styleUrls: ['./partner-view.component.scss']
})
export class PartnerViewComponent implements OnInit {
  public howBecomeData:object = {
    status:'partner',
    title:'who_can_work_with_us',
    items:[{
      image:"assets/partner/hotel.png",
      title:"_hotels_title",
      subtitle:"_hotels_subtitle"
    },
    {
      image:"assets/partner/airport.png",
      title:"_airport_title",
      subtitle:"_airport_subtitle"
    },
    {
      image:"assets/partner/travel.png",
      title:"_travel_agency_title",
      subtitle:"_travel_agency_subtitle"
    }]
  }
  constructor(private _meta:Meta,private _title:Title,private _titleService:TitleService) { 
    setTimeout(() => {
      _title.setTitle(_titleService.titlesInCurrentLanguage['hotelsB2bToursOperators'])
    });
  }
  ngOnInit() {
    this._title.setTitle("Become a Partner")

  }

}
