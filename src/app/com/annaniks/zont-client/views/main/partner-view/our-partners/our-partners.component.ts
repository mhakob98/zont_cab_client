import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'our-partners',
  templateUrl: './our-partners.component.html',
  styleUrls: ['./our-partners.component.scss']
})
export class OurPartnersComponent implements OnInit {
  public partners:Array<string>=[
    "assets/partner/partner1.png",
    "assets/partner/partner2.png",
    "assets/partner/partner3.png",
    "assets/partner/partner4.png",
    "assets/partner/partner5.png",
    "assets/partner/partner6.png",
  ]
  constructor() { }

  ngOnInit() {
  }

}
