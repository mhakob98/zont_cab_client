import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { SharedModule } from "../../../shared/shared.module";
import { PartnerRoutingModule } from "./partner-view.routing.module";
import { PartnerViewComponent } from "./partner-view.component";
import { ImproveWithZontComponent } from './improve-with-zont/improve-with-zont.component';
import { OurPartnersComponent } from './our-partners/our-partners.component';
import { KeyFeauturesComponent } from './key-feautures/key-feautures.component';

@NgModule({
    declarations:[PartnerViewComponent, ImproveWithZontComponent, OurPartnersComponent, KeyFeauturesComponent] ,
    imports:[PartnerRoutingModule,CommonModule,SharedModule],
    exports:[]
})
export class PartnerViewModule{}