import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PartnerViewComponent } from './partner-view.component';

let partnerRoutes: Routes = [
    {path:'',component:PartnerViewComponent}
]

@NgModule({
    imports: [RouterModule.forChild(partnerRoutes)],
    exports: [RouterModule]
})
export class PartnerRoutingModule {}