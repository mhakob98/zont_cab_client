import { PartnerSignUpModal } from './../../../../modals/partner-sign-up/partner-sign-up.modal';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SignInModal } from '../../../../modals/sign-in/sign-in.modal';

@Component({
  selector: 'improve-with-zont',
  templateUrl: './improve-with-zont.component.html',
  styleUrls: ['./improve-with-zont.component.scss'],
})
export class ImproveWithZontComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }
  private _openSignInDialog(): void {    
    const dialogRef = this.dialog.open(SignInModal, {
      width: '1100px',
      panelClass:'sign_in_modal'
    });
    dialogRef.afterClosed().subscribe(result => {

    });
   
  }
  public openPartnerSignUpDialog(): void {
    const dialogRef = this.dialog.open(PartnerSignUpModal,{
      panelClass:'sign_in_modal_partner'
    })
    dialogRef.afterClosed().subscribe(result => {
      if(result==false){
        this._openSignInDialog();
      }
    });
  }
}
