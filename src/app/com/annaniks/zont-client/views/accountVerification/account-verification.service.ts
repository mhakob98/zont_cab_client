import { Injectable } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Observable } from 'rxjs';
import { Role } from '../../models/models';
@Injectable()
export class AccountVerificationService{
    constructor(private _apiService:ApiService){}
    
    public getRefreshToken(code:string) {
        return this._apiService.get('/verification/verify/' + code);
    }
    public getAccessToken(refresh:string) {
        return this._apiService.post('/Login/getaccesstoken?refreshToken=' + refresh, null);
    }   
    
    public getRole(loginToken:string):Observable<Role>{
        return this._apiService.get("/login/getrole",true,null,null,loginToken)
    }

}