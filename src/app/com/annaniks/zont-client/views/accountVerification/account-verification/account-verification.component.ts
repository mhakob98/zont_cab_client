import { AccountVerificationService } from './../account-verification.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Role } from '../../../models/models';
import { CookieService } from 'ngx-cookie-service';

@Component({
  templateUrl: './account-verification.component.html',
  styleUrls: ['./account-verification.component.scss'],
})
export class AccountVerificationComponent implements OnInit {

  private _verificationCode: string = "";
  private _verifyError: boolean = false;
  constructor(
      private _accountVerificationService: AccountVerificationService,
      private _activatedRoute: ActivatedRoute,
      private router: Router,
      private _cookieService: CookieService,
  ) { }

  ngOnInit() {
      this._checkAccountVerification();
    }
    
    private _checkAccountVerification() {      
      this._activatedRoute.params.subscribe((params) => {
          this._verificationCode = params['id'];
          this._accountVerificationService.getRefreshToken(this._verificationCode).subscribe((response: any) => {
              this._accountVerificationService.getAccessToken(response.refreshToken).subscribe((access: any) => {
                  this._accountVerificationService.getRole(access.accessToken).subscribe((role: any) => {
                      if (role.role.toLowerCase() == "client") {
                          this._setCookies(role.role + "_role", role.role);
                          this._setCookies(role.role + "_accessToken", access.accessToken)
                          this._setCookies(role.role + "_refreshToken", access.refreshToken)
                          window.location.href = "http://zont.cab"
                      }
                      else {
                        this._setCookies(role.role + "_role", role.role, "zont.cab");
                        this._setCookies(role.role + "_accessToken", access.accessToken, "zont.cab")
                        this._setCookies(role.role + "_refreshToken", access.refreshToken, "zont.cab")
                        this._decideHrefLocation(role);
                  }   
                  })
              })
          }, (error) => {
              this._verifyError = true;
          })
      })
  }
  private _setCookies(key: string, value: string, domain: string = ''): void {
    this._cookieService.set(key, value,null,null, domain);
  }
  private _decideHrefLocation(role: Role): void {
    role.role.toLowerCase() == "partner" ? window.location.href = 'http://' + role.role.toLowerCase() + '.zont.cab/dashboard/default' :
      window.location.href = 'http://' + role.role.toLowerCase() + '.zont.cab/'
  }
  get verifyError():boolean{
    return this._verifyError;
  }
}
