import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountVerificationComponent } from './account-verification/account-verification.component';

let verificationRoutes: Routes = [
    {
        path: '', component:AccountVerificationComponent 
    }
]

@NgModule({
    imports: [RouterModule.forChild(verificationRoutes)],
    exports: [RouterModule]
})
export class AccountVerificationRoutingModule { }