import { AccountVerificationService } from './account-verification.service';
import { NgModule } from '@angular/core';
import { AccountVerificationComponent } from './account-verification/account-verification.component';
import { CommonModule } from '@angular/common';
import { AccountVerificationRoutingModule } from './account-verification-routing.module';
@NgModule({
    declarations:[AccountVerificationComponent],
    imports:[CommonModule,AccountVerificationRoutingModule],
    exports:[],
    providers:[AccountVerificationService]
})
export class AccountVerificationModule{}