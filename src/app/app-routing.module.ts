import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './com/annaniks/zont-client/services/authguard.service';

let appRoutes: Routes = [
  { path: '', loadChildren: 'src/app/com/annaniks/zont-client/views/main/main.module#MainModule', canActivate: [AuthGuard] },
  { path: 'verify/:id', loadChildren: 'src/app/com/annaniks/zont-client/views/accountVerification/account-verification.module#AccountVerificationModule' },
]
@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { initialNavigation: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
